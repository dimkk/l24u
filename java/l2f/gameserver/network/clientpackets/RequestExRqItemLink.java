package l2f.gameserver.network.clientpackets;

import l2f.gameserver.cache.ItemInfoCache;
import l2f.gameserver.model.Player;
import l2f.gameserver.model.World;
import l2f.gameserver.model.items.ItemInfo;
import l2f.gameserver.network.serverpackets.ActionFail;
import l2f.gameserver.network.serverpackets.ExRpItemLink;
import l2f.gameserver.network.serverpackets.components.CustomMessage;
import l2f.gameserver.utils.l24u.LFAddon;

public class RequestExRqItemLink extends L2GameClientPacket
{
	private int _objectId;

	@Override
	protected void readImpl()
	{
		_objectId = readD();
	}

	@Override
	protected void runImpl()
	{
		ItemInfo item;
		Player player = getClient().getActiveChar(); //clicked
		Player lfUser = World.getPlayer(_objectId);

		if (lfUser != null && LFAddon.lfmUsers.contains(_objectId)){
			if (lfUser.isPartyLeader() || lfUser.getParty() == null){
				if (LFAddon.fastAnswerToPartyRequest(player, lfUser))
					LFAddon.lfmUsers.remove(new Integer(_objectId));
			} else {
				player.sendMessage(new CustomMessage("Пользователь не лидер группы"));
				lfUser.sendMessage(new CustomMessage("Вы должны быть лидером группы (или без группы), чтобы принимать людей"));
			}
		}
		if (lfUser != null && LFAddon.lfpUsers.contains(_objectId)){
			if (player.isPartyLeader() || player.getParty() == null) {
				if (LFAddon.fastAnswerToPartyRequest(lfUser, player))
					LFAddon.lfpUsers.remove(new Integer(_objectId));
			}else {
				lfUser.sendMessage(new CustomMessage("Пользователь не лидер группы"));
				player.sendMessage(new CustomMessage("Вы должны быть лидером группы (или без группы), чтобы принимать людей"));
			}
		}

		if ((item = ItemInfoCache.getInstance().get(_objectId)) == null)
		{
			if (_objectId >= 5000000 && _objectId < 6000000 && getClient().getActiveChar() != null)
			{

				String varName = "DisabledAnnounce" + _objectId;
				if (!player.containsQuickVar(varName))
				{
					player.addQuickVar(varName, "true");
					player.sendMessage("Announcement Disabled!");
				}
			}
			sendPacket(ActionFail.STATIC);
		}
		else
			sendPacket(new ExRpItemLink(item));
	}
}