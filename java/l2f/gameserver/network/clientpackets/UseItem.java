package l2f.gameserver.network.clientpackets;

import l2f.gameserver.Config;
import l2f.gameserver.model.Player;
import l2f.gameserver.model.base.TeamType;
import l2f.gameserver.model.items.ItemInstance;
import l2f.gameserver.network.ClientHelpers;
import l2f.gameserver.network.serverpackets.ExUseSharedGroupItem;
import l2f.gameserver.network.serverpackets.SystemMessage2;
import l2f.gameserver.network.serverpackets.components.CustomMessage;
import l2f.gameserver.network.serverpackets.components.SystemMsg;
import l2f.gameserver.skills.TimeStamp;
import l2f.gameserver.tables.PetDataTable;

import org.apache.commons.lang3.ArrayUtils;

public class UseItem extends L2GameClientPacket
{
	private int _objectId;
	private boolean _ctrlPressed;
	private long _timeSent;

	@Override
	protected void readImpl()
	{
		_objectId = readD();
		_ctrlPressed = readD() == 1;
		_timeSent = System.currentTimeMillis();
	}

	@Override
	protected void runImpl()
	{
		Player activeChar = getClient().getActiveChar();
		ClientHelpers.UseItem(activeChar, _objectId, _ctrlPressed);
	}
}