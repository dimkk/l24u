package l2f.gameserver.network;

import l2f.commons.math.SafeMath;
import l2f.gameserver.Config;
import l2f.gameserver.handler.bbs.CommunityBoardManager;
import l2f.gameserver.model.Player;
import l2f.gameserver.model.Zone;
import l2f.gameserver.model.base.TeamType;
import l2f.gameserver.model.items.ItemInstance;
import l2f.gameserver.model.items.PcInventory;
import l2f.gameserver.model.items.Warehouse;
import l2f.gameserver.network.serverpackets.ExBuySellList;
import l2f.gameserver.network.serverpackets.ExUseSharedGroupItem;
import l2f.gameserver.network.serverpackets.SystemMessage2;
import l2f.gameserver.network.serverpackets.components.CustomMessage;
import l2f.gameserver.network.serverpackets.components.SystemMsg;
import l2f.gameserver.skills.TimeStamp;
import l2f.gameserver.tables.PetDataTable;
import l2f.gameserver.templates.item.ItemTemplate;
import l2f.gameserver.utils.Location;
import org.apache.commons.lang3.ArrayUtils;

/**
 * Created by dimkk on 2017-03-12.
 */
public class ClientHelpers {
    public static void DropItem (Player activeChar, Long _count, Location _loc, Integer _objectId){
        if (activeChar == null)
            return;

        if (_count < 1 || _loc.isNull())
        {
            activeChar.sendActionFailed();
            return;
        }

        if (activeChar.isActionsDisabled() || activeChar.isBlocked())
        {
            activeChar.sendActionFailed();
            return;
        }

        if (!Config.ALLOW_DISCARDITEM)
        {
            activeChar.sendMessage(new CustomMessage("l2f.gameserver.clientpackets.RequestDropItem.Disallowed", activeChar));
            return;
        }

        if (activeChar.isInStoreMode())
        {
            activeChar.sendPacket(SystemMsg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
            return;
        }

        if (activeChar.isSitting() || activeChar.isDropDisabled())
        {
            activeChar.sendActionFailed();
            return;
        }

        if (activeChar.isInTrade())
        {
            activeChar.sendActionFailed();
            return;
        }

        if (activeChar.isFishing())
        {
            activeChar.sendPacket(SystemMsg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
            return;
        }

        if (!activeChar.isInRangeSq(_loc, 22500) || Math.abs(_loc.z - activeChar.getZ()) > 50)
        {
            activeChar.sendPacket(SystemMsg.YOU_CANNOT_DISCARD_SOMETHING_THAT_FAR_AWAY_FROM_YOU);
            return;
        }

        ItemInstance item = activeChar.getInventory().getItemByObjectId(_objectId);
        if (item == null)
        {
            activeChar.sendActionFailed();
            return;
        }

        if (!item.canBeDropped(activeChar, false))
        {
            activeChar.sendPacket(SystemMsg.THAT_ITEM_CANNOT_BE_DISCARDED);
            return;
        }

        if (activeChar.isInZone(Zone.ZoneType.SIEGE) || item.getAttachment() != null)
        {
            activeChar.sendMessage("Cannot drop items in Siege Zone!");
            return;
        }

        item.getTemplate().getHandler().dropItem(activeChar, item, _count, _loc);
    }

    public static void UseItem(Player activeChar, int _objectId, boolean _ctrlPressed) {
        if (activeChar == null)
            return;

        activeChar.setActive();

        ItemInstance item = activeChar.getInventory().getItemByObjectId(_objectId);
        if (item == null)
        {
            activeChar.sendActionFailed();
            return;
        }

        int itemId = item.getItemId();

        if (activeChar.isInStoreMode())
        {
            if (PetDataTable.isPetControlItem(item))
                activeChar.sendPacket(SystemMsg.YOU_CANNOT_SUMMON_DURING_A_TRADE_OR_WHILE_USING_A_PRIVATE_STORE);
            else
                activeChar.sendPacket(SystemMsg.YOU_MAY_NOT_USE_ITEMS_IN_A_PRIVATE_STORE_OR_PRIVATE_WORK_SHOP);
            return;
        }

        if (activeChar.isFishing() && (itemId < 6535 || itemId > 6540))
        {
            activeChar.sendPacket(SystemMsg.YOU_CANNOT_DO_THAT_WHILE_FISHING_2);
            return;
        }

		/*if(activeChar._event != null && activeChar._event.getName().equalsIgnoreCase("Capture The Flag") && (itemId == 13560 || itemId == 13561))
		{
			activeChar.sendMessage(new CustomMessage("l2f.gameserver.network.l2.c2s.UseItem.NotUseIsEvents", activeChar));
			return;
		}*/

        if (ArrayUtils.contains(Config.ITEM_USE_LIST_ID, itemId) && !Config.ITEM_USE_IS_COMBAT_FLAG && (activeChar.getPvpFlag() != 0 || activeChar.isInDuel() || activeChar.isInCombat()))
        {
            activeChar.sendMessage(new CustomMessage("l2f.gameserver.network.l2.c2s.UseItem.NotUseIsFlag", activeChar));
            return;
        }

		/*if(ArrayUtils.contains(Config.ITEM_USE_LIST_ID, itemId) && !Config.ITEM_USE_IS_EVENTS && Events.onAction(activeChar, activeChar, true))
		{
			activeChar.sendMessage(new CustomMessage("l2f.gameserver.network.l2.c2s.UseItem.NotUseIsEvents", activeChar));
			return;
		}*/

        if (ArrayUtils.contains(Config.ITEM_USE_LIST_ID, itemId) && !Config.ITEM_USE_IS_ATTACK && activeChar.isAttackingNow())
        {
            activeChar.sendMessage(new CustomMessage("l2f.gameserver.network.l2.c2s.UseItem.NotUseIsFlag", activeChar));
            return;
        }

        if (activeChar.isSharedGroupDisabled(item.getTemplate().getReuseGroup()))
        {
            activeChar.sendReuseMessage(item);
            return;
        }

        if (!item.getTemplate().testCondition(activeChar, item))
            return;

        if (activeChar.getInventory().isLockedItem(item))
            return;

        if (item.getTemplate().isForPet())
        {
            activeChar.sendPacket(SystemMsg.YOU_MAY_NOT_EQUIP_A_PET_ITEM);
            return;
        }

        // Baby Buffalo Improved
        if (Config.ALT_IMPROVED_PETS_LIMITED_USE && activeChar.isMageClass() && item.getItemId() == 10311)
        {
            activeChar.sendPacket(new SystemMessage2(SystemMsg.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(itemId));
            return;
        }

        // Improved Baby Kookaburra
        if (Config.ALT_IMPROVED_PETS_LIMITED_USE && !activeChar.isMageClass() && item.getItemId() == 10313)
        {
            activeChar.sendPacket(new SystemMessage2(SystemMsg.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(itemId));
            return;
        }

        if (activeChar.isOutOfControl())
        {
            activeChar.sendActionFailed();
            return;
        }

        if ((item.getItemId() == 3936 || item.getItemId() == 1540) && activeChar.getTeam() != TeamType.NONE)
        {
            activeChar.sendMessage("Cannot use that during Event!");
            return;
        }

        boolean success = item.getTemplate().getHandler().useItem(activeChar, item, _ctrlPressed);
        if (success)
        {
            long nextTimeUse = item.getTemplate().getReuseType().next(item);
            if (nextTimeUse > System.currentTimeMillis())
            {
                TimeStamp timeStamp = new TimeStamp(item.getItemId(), nextTimeUse, item.getTemplate().getReuseDelay());
                activeChar.addSharedGroupReuse(item.getTemplate().getReuseGroup(), timeStamp);

                if (item.getTemplate().getReuseDelay() > 0)
                    activeChar.sendPacket(new ExUseSharedGroupItem(item.getTemplate().getDisplayReuseGroup(), timeStamp));
            }
        }
    }

    public static void SellItem(Player activeChar, int _count, int[] _items, long[] _itemQ) {
        if (activeChar == null || _count == 0)
            return;

        if (activeChar.isActionsDisabled() || activeChar.isBlocked())
        {
            activeChar.sendActionFailed();
            return;
        }

        if (activeChar.isInStoreMode())
        {
            activeChar.sendPacket(SystemMsg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
            return;
        }

        if (activeChar.isInTrade())
        {
            activeChar.sendActionFailed();
            return;
        }

        if (activeChar.isFishing())
        {
            activeChar.sendPacket(SystemMsg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
            return;
        }

        if (!Config.ALT_GAME_KARMA_PLAYER_CAN_SHOP && activeChar.getKarma() > 0 && !activeChar.isGM())
        {
            activeChar.sendActionFailed();
            return;
        }

        activeChar.getInventory().writeLock();
        try
        {
            for (int i = 0; i < _count; i++)
            {
                int objectId = _items[i];
                long count = _itemQ[i];

                ItemInstance item = activeChar.getInventory().getItemByObjectId(objectId);
                if (item == null || item.getCount() < count || !item.canBeSold(activeChar))
                    continue;


                //TODO понять, учитывается ли налог? Пока, как будто нет.
                long price = SafeMath.mulAndCheck(item.getReferencePrice(), count) / 2;
                if (Config.SELL_ALL_ITEMS_FREE)
                    price = 1;
                ItemInstance refund = activeChar.getInventory().removeItemByObjectId(objectId, count, "Selling Item");

                activeChar.addAdena(price, true, "Selling Item");
                activeChar.getRefund().addItem(refund, null, null);
                if (activeChar.isBBSUse())
                    activeChar.setIsBBSUse(false);
            }
        }
        catch (ArithmeticException ae)
        {
            activeChar.sendPacket(SystemMsg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);
            return;
        }
        finally
        {
            activeChar.getInventory().writeUnlock();
        }

        activeChar.sendPacket(new ExBuySellList.SellRefundList(activeChar, true));
        activeChar.sendChanges();
    }

    public static void DepositItems(Player activeChar, int _count, int[] _items, long[] _itemQ, long _WAREHOUSE_FEE) {
        if (activeChar == null || _count == 0)
            return;

        if (!activeChar.getPlayerAccess().UseWarehouse)
        {
            activeChar.sendActionFailed();
            return;
        }

        if (activeChar.isActionsDisabled())
        {
            activeChar.sendActionFailed();
            return;
        }

        if (activeChar.isInStoreMode())
        {
            activeChar.sendPacket(SystemMsg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
            return;
        }

        if (activeChar.isInTrade())
        {
            activeChar.sendActionFailed();
            return;
        }

        if (activeChar.getUsingWarehouseType() == Warehouse.WarehouseType.FREIGHT)
        {
            checkAuctionAdd(activeChar, _items, _itemQ);
            return;
        }

        PcInventory inventory = activeChar.getInventory();
        boolean privatewh = activeChar.getUsingWarehouseType() != Warehouse.WarehouseType.CLAN;
        Warehouse warehouse;
        if (privatewh)
            warehouse = activeChar.getWarehouse();
        else
            warehouse = activeChar.getClan().getWarehouse();

        inventory.writeLock();
        warehouse.writeLock();
        try
        {
            int slotsleft = 0;
            long adenaDeposit = 0;

            if (privatewh)
                slotsleft = activeChar.getWarehouseLimit() - warehouse.getSize();
            else
                slotsleft = activeChar.getClan().getWhBonus() + Config.WAREHOUSE_SLOTS_CLAN - warehouse.getSize();

            int items = 0;

            // Создаем новый список передаваемых предметов, на основе полученных данных
            for (int i = 0; i < _count; i++)
            {
                ItemInstance item = inventory.getItemByObjectId(_items[i]);
                if (item == null || item.getCount() < _itemQ[i] || !item.canBeStored(activeChar, privatewh || !privatewh))
                {
                    _items[i] = 0; // Обнуляем, вещь не будет передана
                    _itemQ[i] = 0L;
                    continue;
                }

                if (!item.isStackable() || warehouse.getItemByItemId(item.getItemId()) == null) // вещь требует слота
                {
                    if (slotsleft <= 0) // если слоты кончились нестекуемые вещи и отсутствующие стекуемые пропускаем
                    {
                        _items[i] = 0; // Обнуляем, вещь не будет передана
                        _itemQ[i] = 0L;
                        continue;
                    }
                    slotsleft--; // если слот есть то его уже нет
                }

                if (item.getItemId() == ItemTemplate.ITEM_ID_ADENA)
                    adenaDeposit = _itemQ[i];

                items++;
            }

            // Сообщаем о том, что слоты кончились
            if (slotsleft <= 0)
                activeChar.sendPacket(SystemMsg.YOUR_WAREHOUSE_IS_FULL);

            if (items == 0)
            {
                activeChar.sendPacket(SystemMsg.INCORRECT_ITEM_COUNT);
                return;
            }

            // Проверяем, хватит ли у нас денег на уплату налога
            long fee = SafeMath.mulAndCheck(items, _WAREHOUSE_FEE);

            if (fee + adenaDeposit > activeChar.getAdena())
            {
                activeChar.sendPacket(SystemMsg.YOU_LACK_THE_FUNDS_NEEDED_TO_PAY_FOR_THIS_TRANSACTION);
                return;
            }

            if (!activeChar.reduceAdena(fee, true, (privatewh ? "Private" : "Clan")+"WarehouseDepositFee"))
            {
                activeChar.sendPacket(SystemMsg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
                return;
            }

            for (int i = 0; i < _count; i++)
            {
                if (_items[i] == 0)
                    continue;
                ItemInstance item = inventory.removeItemByObjectId(_items[i], _itemQ[i], (privatewh ? "Private" : "Clan")+"WarehouseDeposit");
                warehouse.addItem(item, null, null);
            }
        }
        catch (ArithmeticException ae)
        {
            // TODO audit
            activeChar.sendPacket(SystemMsg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);
            return;
        }
        finally
        {
            warehouse.writeUnlock();
            inventory.writeUnlock();
        }

        // Обновляем параметры персонажа
        activeChar.sendChanges();
        activeChar.sendPacket(SystemMsg.THE_TRANSACTION_IS_COMPLETE);
    }

    private static void  checkAuctionAdd(Player activeChar, int[] _items, long[] _itemQ)
    {
        if (_items.length != 1 || _itemQ[0] != 1)
        {
            activeChar.sendMessage("You can add just one item at the time!");
            return;
        }

        CommunityBoardManager.getInstance().getCommunityHandler("_sendTimePrice_").onBypassCommand(activeChar, "_sendTimePrice_"+_items[0]);
    }
}
