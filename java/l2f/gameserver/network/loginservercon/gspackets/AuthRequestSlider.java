package l2f.gameserver.network.loginservercon.gspackets;

import l2f.commons.net.AdvIP;
import l2f.gameserver.Config;
import l2f.gameserver.GameServer;
import l2f.gameserver.network.loginservercon.SendablePacket;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class AuthRequestSlider extends SendablePacket
{
	private Integer getId(){
		List<String> ids = new LinkedList<String>(Arrays.asList(Config.L24U_SLIDERS_IDS));
		if (ids.size() > 0) {
			Integer result = Integer.parseInt(ids.get(0));
			ids.remove(0);
			String[] stockArr = new String[ids.size()];
			Config.L24U_SLIDERS_IDS = ids.toArray(stockArr);
			return result;
		} else {
			return -1;
		}

	}
	protected void writeImpl()
	{
		writeC(0x00);
		writeD(GameServer.AUTH_SERVER_PROTOCOL);
		writeC(getId());
		writeC(Config.ACCEPT_ALTERNATE_ID ? 0x01 : 0x00);
		writeD(Config.AUTH_SERVER_SERVER_TYPE);
		writeD(Config.AUTH_SERVER_AGE_LIMIT);
		writeC(Config.AUTH_SERVER_GM_ONLY ? 0x01 : 0x00);
		writeC(Config.AUTH_SERVER_BRACKETS ? 0x01 : 0x00);
		writeC(Config.AUTH_SERVER_IS_PVP ? 0x01 : 0x00);
		writeS(Config.EXTERNAL_HOSTNAME);
		writeS(Config.INTERNAL_HOSTNAME);
		writeH(Config.PORTS_GAME.length);
		for (int PORT_GAME : Config.PORTS_GAME)
			writeH(PORT_GAME);
		writeD(Config.MAXIMUM_ONLINE_USERS);
		writeD(Config.GAMEIPS.size());
		for (AdvIP ip : Config.GAMEIPS)
		{
			writeS(ip.ipadress);
			writeS(ip.ipmask);
			writeS(ip.bitmask);
		}
	}
}
