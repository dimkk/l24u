package l2f.gameserver.utils.l24u;

import l2f.commons.threading.RunnableImpl;
import l2f.gameserver.Config;
import l2f.gameserver.ThreadPoolManager;
import l2f.gameserver.model.Player;
import l2f.gameserver.model.World;
import l2f.gameserver.model.instances.MonsterInstance;
import l2f.gameserver.model.instances.NpcInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by Dima on 01.09.2016.
 */
public class TormentAddon {

    private static final Logger _log = LoggerFactory.getLogger(TormentAddon.class);

    // Для игрока
    public int _maxTorment = 0;
    public int _surroundingMobs = 0;
    public int _b4SurroundingMobs = 0;
    private Player _activePlayer;

    public TormentAddon(Player activePlayer) {
        _activePlayer = activePlayer;
    }

    public void getMobsToSetTorment(Player activePlayer){

    }
    public void setCheckTask(){
        ThreadPoolManager.getInstance().scheduleAtFixedRate(new setTorment(_activePlayer), 0, Config.TORMENT_CHECK_RATE);
    }

    public class setTorment extends RunnableImpl
    {
        Player _activePlayer;

        public setTorment(Player _activePlayer) {
            this._activePlayer = _activePlayer;
        }

        @Override
        public void runImpl() throws Exception
        {
            int skillId = 0;

            if (Config.TORMENT_STAGE == 1){
                skillId = 90002;
            }
            if (Config.TORMENT_STAGE == 2){
                skillId = 90003;
            }
            if (Config.TORMENT_STAGE == 3){
                skillId = 90004;
            }

            _surroundingMobs = 0;
            List<NpcInstance> npcs =  World.getAroundNpc(_activePlayer, Config.TORMENT_AFFECT_RANGE, Config.TORMENT_AFFECT_RANGE_Z);
            for (NpcInstance npc : npcs){
                if (npc instanceof MonsterInstance){
                    _surroundingMobs++;
                    ((MonsterInstance) npc).tickle(skillId);
                }
            }
//            if (_b4SurroundingMobs != _surroundingMobs){
//                _activePlayer.sendMessage("Вокруг тебя - " + _surroundingMobs + " мобцов");
//                _b4SurroundingMobs = _surroundingMobs;
//            }

        }

        private int getCurrentT(){
            int result = 0;
            String currentTorment = _activePlayer.getSessionVar("torment");
            if (currentTorment != null &&  currentTorment != ""){
                Integer newTCheck;
                try{
                    newTCheck = Integer.parseInt(currentTorment);
                } catch (Exception ex) {
                    return result;
                }
                result = newTCheck;

            }
            return result;
        }
    }

    // Для мобца
    public static int getMaxTormForMob(MonsterInstance mob, int mobLvl){
        int result = 0;
        List<Player> surroundingPlayers =  World.getAroundPlayers(mob, Config.TORMENT_AFFECT_RANGE, Config.TORMENT_AFFECT_RANGE_Z);
        result = getMaxTorment(surroundingPlayers, result, mobLvl);
        return result;
    }
    private static int getMaxTorment(List<Player> surroundingPlayers, int maxTorment, int mobLvl) {
        int aroundPlayersTor = -1;
        int maxSetTor = 0;
        for (Player player : surroundingPlayers){
            String currentPlayerTor = player.getSessionVar("torment");
            if (currentPlayerTor != null){
                int currentPlayerTorInt = 0;
                try
                {
                    currentPlayerTorInt = Integer.parseInt(currentPlayerTor);
                } catch (Exception ex){
                    _log.error(ex.toString());
                }

                if (currentPlayerTorInt > 0 && currentPlayerTorInt > maxTorment && currentPlayerTorInt < Config.TORMENT_LVLS){
                    maxTorment = currentPlayerTorInt;
                }
            }
            if (Math.abs(player.getLevel() - mobLvl) <= Config.TORMENT_MOB_PLAYER_MAX_DIFF && (aroundPlayersTor - 1) < Config.TORMENT_LVLS){
                aroundPlayersTor++;
            }
        }
        if (aroundPlayersTor > maxTorment) maxTorment = aroundPlayersTor;
        return maxTorment;
    }
}
