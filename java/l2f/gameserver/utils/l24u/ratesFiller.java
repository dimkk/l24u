package l2f.gameserver.utils.l24u;

import l2f.gameserver.Config;
import org.apache.commons.io.FileUtils;
import org.apache.commons.pool.impl.GenericKeyedObjectPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rGuard.ConfigProtection;

import java.io.File;
import java.io.IOException;

/**
 * Created by dimkk on 2017-02-12.
 */
public class ratesFiller {
    protected static final Logger _log = LoggerFactory.getLogger("l24uCommon.Filler");
    static String exp = intValueToString(Config.RATE_XP);
    static String sp = intValueToString(Config.RATE_SP);
    static String adena = intValueToString(Config.RATE_DROP_ADENA);
    static String drop = intValueToString(Config.RATE_DROP_ITEMS);
    static String spoil = intValueToString(Config.RATE_CHANCE_SPOIL);
    static String questDrop = intValueToString(Config.RATE_QUESTS_DROP);
    static String enchantSafeMax = intValueToString(Config.SAFE_ENCHANT_COMMON);
    static String enchantMax = intValueToString(Config.ENCHANT_MAX);
    static String startScrollEnchantChance = intValueToString(Config.ENCHANT_CHANCE_WEAPON);
    static String startBlessScrollEnchantChance = intValueToString(Config.ENCHANT_CHANCE_WEAPON_BLESS);
    static String attrChance = intValueToString(Config.ENCHANT_ATTRIBUTE_STONE_CHANCE);
    static String attr300Chance = intValueToString(Config.ENCHANT_ATTRIBUTE_CRYSTAL_CHANCE);
    static String windowCount = intValueToString(ConfigProtection.PROTECT_WINDOWS_COUNT);

    public static void doFill(){
        try {
            fillAnnounceTxt();
            fillServerInfoHtml();
        } catch (Exception e) {
            _log.error("ex", e);
        }

    }
    private static void fillAnnounceTxt() throws IOException {
        String contents = FileUtils.readFileToString(new File("./config/announcements.tpl"), "UTF-8");
        contents = contents
                .replace("${exp}", exp)
                .replace("${sp}", sp)
                .replace("${adena}", adena)
                .replace("${drop}", drop)
                .replace("${spoil}", spoil);
        FileUtils.writeStringToFile(new File("./config/announcements.txt"), contents, "UTF-8");
    };

    private static void fillServerInfoHtml() throws IOException {
        String contents = FileUtils.readFileToString(new File("./html-ru/scripts/services/communityPVP/pages/Serverinfo.htm.tpl"), "UTF-8");
        contents = contents
                .replace("${exp}", exp)
                .replace("${adena}", adena)
                .replace("${drop}", drop)
                .replace("${questItems}", questDrop)
                .replace("${enchantSafeMax}", enchantSafeMax)
                .replace("${enchantMax}", enchantMax)
                .replace("${startScrollEnchantChance}", startScrollEnchantChance)
                .replace("${startBlessScrollEnchantChance}", startBlessScrollEnchantChance)
                .replace("${attrChance}", attrChance)
                .replace("${attr300Chance}", attr300Chance)
                .replace("${windowCount}", windowCount);
        FileUtils.writeStringToFile(new File("./html-ru/scripts/services/communityPVP/pages/Serverinfo.htm"), contents, "UTF-8");

    }

    private static String intValueToString(double d) {
        return ((Integer)(int)d).toString();
    }
}
