package l2f.gameserver.utils.l24u;

import l2f.gameserver.model.Party;
import l2f.gameserver.model.Player;
import l2f.gameserver.model.Request;
import l2f.gameserver.network.serverpackets.ActionFail;
import l2f.gameserver.network.serverpackets.JoinParty;
import l2f.gameserver.network.serverpackets.components.IStaticPacket;
import l2f.gameserver.network.serverpackets.components.SystemMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class LFAddon
{
	private static final Logger log = LoggerFactory.getLogger(LFAddon.class);
    public static List<Integer> lfmUsers = new ArrayList<Integer>();
    public static List<Integer> lfpUsers = new ArrayList<Integer>();

    public static String isLFPOk(Player activeChar){
        if (activeChar.getParty() != null) return "Вы не должны находится в пати!";
        return "ok";
    }

    public static String isLFMOk(Player activeChar){
        if (activeChar.getParty() != null && activeChar.getParty().getPartyLeader() != activeChar) return "Вы должны быть лидером группы чтобы искать людей";
        return "ok";
    }

    public static String getLFPMessage(Player activeChar)
	{
        // TODO Добавить интернационализации
        int activObjId = activeChar.getObjectId();
        // Проверим что чувак не в ищет людей в пати - если ищет - уберем его оттуда
        if (lfmUsers.contains(activObjId)) lfmUsers.remove(new Integer(activObjId));

        // Добавим в список
        if (!lfpUsers.contains(activObjId)) lfpUsers.add(activObjId);


        String message = "\b\tType=1 \tID="+activObjId+" \tColor=0 \tUnderline=0 \tTitle=\u001BИщу Пати - ";
        message = message + activeChar.getActiveClass().toString();
        message = message + "\u001B\b";
        return message;
    }

    public static String getLFMMessage(Player activeChar)
    {
        // TODO интернационализации
        int activObjId = activeChar.getObjectId();
        if (lfpUsers.contains(activObjId)) lfpUsers.remove(new Integer(activObjId));
        if (!lfmUsers.contains(activObjId)) lfmUsers.add(activObjId);
        int currentPartyMemebers = 1;
        double currentPartyLevel = activeChar.getLevel();
        if (activeChar.getParty() != null){
            currentPartyMemebers = activeChar.getParty().getMemberCount();
            currentPartyLevel = activeChar.getParty().getAvLevel();
        }
        String message = "\b\tType=1 \tID="+activObjId+" \tColor=0 \tUnderline=0 \tTitle=\u001BИщем людей в пати - ";
        message = message + currentPartyMemebers + " / 9, ур. - " + currentPartyLevel;
        message = message + "\u001B\b";
        return message;
    }

    public static boolean fastAnswerToPartyRequest(Player activeChar, Player requestor){
        if (activeChar == null)
            return false;

        if (activeChar.isOutOfControl())
        {
            activeChar.sendActionFailed();
            return false;
        }

        if (requestor == null)
        {
            activeChar.sendPacket(SystemMsg.THAT_PLAYER_IS_NOT_ONLINE);
            activeChar.sendActionFailed();
            return false;
        }

        if (activeChar.isInOlympiadMode())
        {
            activeChar.sendPacket(SystemMsg.A_PARTY_CANNOT_BE_FORMED_IN_THIS_AREA);
            requestor.sendPacket(JoinParty.FAIL);
            return false;
        }

        if (requestor.isInOlympiadMode())
        {
            requestor.sendPacket(JoinParty.FAIL);
            return false;
        }

        Party party = requestor.getParty();

        if (party != null && party.getMemberCount() >= Party.MAX_SIZE)
        {
            activeChar.sendPacket(SystemMsg.THE_PARTY_IS_FULL);
            requestor.sendPacket(SystemMsg.THE_PARTY_IS_FULL);
            requestor.sendPacket(JoinParty.FAIL);
            return false;
        }

        IStaticPacket problem = activeChar.canJoinParty(requestor);
        if (problem != null)
        {
            activeChar.sendPacket(problem, ActionFail.STATIC);
            requestor.sendPacket(JoinParty.FAIL);
            return false;
        }

        if (party == null)
        {
            //TODO 0 - itemDistribution - определять
            int itemDistribution = 0;
            requestor.setParty(party = new Party(requestor, itemDistribution));

        }

        try
        {
            activeChar.joinParty(party);
            requestor.sendPacket(JoinParty.SUCCESS);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }
}
