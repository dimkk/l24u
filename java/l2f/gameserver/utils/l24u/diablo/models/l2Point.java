package l2f.gameserver.utils.l24u.diablo.models;

import l2f.gameserver.utils.Location;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

/**
 * Created by dimkk on 11/25/2016.
 */
@Entity("l2Point")
@Indexes(
        @Index(value = "x", fields = @Field("x"))
)
public class l2Point {
    public l2Point(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public l2Point(){}

    @Id
    private ObjectId id;

    private int x;
    private int y;
    private int z;

    public ObjectId getId() {
        return id;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public Location toLocation() {
        return new Location(x, y, z);
    }
    @Override
    public final String toString()
    {
        return x + "," + y + "," + z ;
    }

}
