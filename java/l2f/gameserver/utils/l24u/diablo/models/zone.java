package l2f.gameserver.utils.l24u.diablo.models;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dimkk on 11/25/2016.
 */
@Entity("zone")
@Indexes(
        @Index(value = "title", fields = @Field("title"))
)
public class zone {
    @Id
    private ObjectId id;
    @Property("title")
    private String title;
    @Property("maxZ")
    private Integer maxZ;
    @Property("minZ")
    private Integer minZ;
    @Reference
    private List<l2Point> points;
    @Reference
    private l2Point StartNpcLocation;
    @Reference
    private l2Point EndNpcLocation;
    @Property("updateDate")
    private Date updateDate;
    @Property("mobsCount")
    private Integer mobsCount;

    //communication
    public String type = "get";
    public int skip = 0;
    public int size = 20;
    public String getId;

    public zone(String title, List<l2Point> points, l2Point startNpcLocation, l2Point endNpcLocation) {
        this.title = title;
        this.points = points;
        StartNpcLocation = startNpcLocation;
        EndNpcLocation = endNpcLocation;
    }
    public zone(String title, List<l2Point> points) {
        this.title = title;
        this.points = points;

    }
    public zone(String title) {
        this.title = title;
        this.points = new ArrayList<l2Point>();
    }
    public zone(List<l2Point> points, Integer maxz, Integer minz) {
        this.points = points;
        this.maxZ = maxz;
        this.minZ = minz;
    }
    public zone() {
    }


    public ObjectId getId() {
        return id;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<l2Point> getPoints() {
        points = points == null ? new ArrayList<l2Point>() : points;
        return points;
    }
    public zone addPoint(l2Point p) {
        this.points.add(p);
        return this;
    }
    public void setPoints(List<l2Point> points) {
        this.points = points;
    }

    public l2Point getStartNpcLocation() {
        return StartNpcLocation;
    }

    public void setStartNpcLocation(l2Point startNpcLocation) {
        StartNpcLocation = startNpcLocation;
    }

    public l2Point getEndNpcLocation() {
        return EndNpcLocation;
    }

    public void setEndNpcLocation(l2Point endNpcLocation) {
        EndNpcLocation = endNpcLocation;
    }

    public Integer getMaxZ() {
        return maxZ;
    }

    public void setMaxZ(Integer maxZ) {
        this.maxZ = maxZ;
    }

    public Integer getMinZ() {
        return minZ;
    }

    public void setMinZ(Integer minZ) {
        this.minZ = minZ;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getMobsCount() {
        return mobsCount;
    }

    public void setMobsCount(Integer mobsCount) {
        this.mobsCount = mobsCount;
    }
}
