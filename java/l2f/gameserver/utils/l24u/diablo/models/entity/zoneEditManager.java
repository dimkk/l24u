package l2f.gameserver.utils.l24u.diablo.models.entity;

import l2f.gameserver.model.Player;
import l2f.gameserver.utils.l24u.diablo.models.l2Point;
import l2f.gameserver.utils.l24u.diablo.models.zone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class zoneEditManager
{
	protected static final Logger _log = LoggerFactory.getLogger("zoneEditManager");

	private static zoneEditManager _instance;
	public static zoneEditManager getInstance()
	{
		if (_instance == null)
			_instance = new zoneEditManager();
		return _instance;
	}

	public void moveToPoint(Player p, zone z, String[] args) {
		if (args[0].equalsIgnoreCase("start")) {
			p.teleToLocation(z.getStartNpcLocation().getX(), z.getStartNpcLocation().getY(), z.getStartNpcLocation().getZ());
			return;
		}
		if (args[0].equalsIgnoreCase("end")) {
			p.teleToLocation(z.getEndNpcLocation().getX(), z.getEndNpcLocation().getY(), z.getEndNpcLocation().getZ());
			return;
		}
		int index = Integer.parseInt(args[0]);
		l2Point point = z.getPoints().get(index);
		p.teleToLocation(point.getX(), point.getY(), point.getZ());
	}
}
