package l2f.gameserver.utils.l24u.diablo.models.entity;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import l2f.gameserver.model.Skill;
import l2f.gameserver.utils.l24u.diablo.models.DiabloItem;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dimkk on 1/4/2017.
 */
public class DiabloItemSkill {

        private String name;
        private Integer lvl;
        private String icon;

        public DiabloItemSkill() {
        }

        public DiabloItemSkill(String name, Integer lvl, String icon, Integer id) {
            this.name = name;
            this.lvl = lvl;
            this.icon = icon;
            this.id = id;
        }

        private Integer id;

    public static DiabloItemSkill parse(Skill resultSkill) {
        return new DiabloItemSkill(resultSkill.getName(), resultSkill.getLevel(), resultSkill.getIcon(), resultSkill.getId());
    }

    public static String generateString(List<DiabloItemSkill> rndSkills) {
        Gson gson = new GsonBuilder().create();
        Type listType = new TypeToken<ArrayList<DiabloItemSkill>>() {
        }.getType();
        return gson.toJson(rndSkills, listType);
    }
}
