package l2f.gameserver.utils.l24u.diablo.models;

import com.google.common.base.Strings;
import l2f.gameserver.utils.l24u.diablo.StatsRndManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by dimkk on 12/27/2016.
 */
public class DiabloItemsObj {
    public int itemId;
    public int randomCount;
    public String skillsAndLevels;

    public long getRandomPrice() {
        return randomPrice;
    }

    public void setRandomPrice(long randomPrice) {
        this.randomPrice = randomPrice;
    }

    public long randomPrice;

    public int getRandomCount() {
        return randomCount ;
    }

    public void setRandomCount(int randomCount) {
        this.randomCount = randomCount;
    }

    public DiabloItemsObj(int itemId, String skillsAndLevels) {
        this.itemId = itemId;
        this.skillsAndLevels = skillsAndLevels;
    }
    public DiabloItemsObj(int itemId, String skillsAndLevels, int randomCount) {
        this.itemId = itemId;
        this.randomCount = randomCount;
        this.skillsAndLevels = skillsAndLevels;
    }

    public DiabloItemsObj(int itemId, int skillId, int skillLvl) {
        this.itemId = itemId;
        this.skillsAndLevels = Integer.toString(skillId) + StatsRndManager.levelsSplit + Integer.toString(skillLvl);
    }
    public DiabloItemsObj() {
    }
    public Integer getSkillId(){
        if (!Strings.isNullOrEmpty(skillsAndLevels)){
            return Integer.parseInt(skillsAndLevels.split(StatsRndManager.levelsSplit)[0]);
        } else return -1;
    }
    public Integer getSkillLvl(){
        if (!Strings.isNullOrEmpty(skillsAndLevels)){
            return Integer.parseInt(skillsAndLevels.split(StatsRndManager.levelsSplit)[1]);
        } else return -1;
    }
}
