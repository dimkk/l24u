package l2f.gameserver.utils.l24u.diablo.models;

/**
 * Created by dimkk on 1/6/2017.
 */
public class statsRndParams {
    private Integer[] skillIds;

    private Integer[] pWeapSkills;
    private Integer[] mWeapSkills;

    private Integer[] pHeadSkills;
    private Integer[] mHeadSkills;

    private Integer[] pBodySkills;
    private Integer[] mBodySkills;

    private Integer[] pShoesSkills;
    private Integer[] mShoesSkills;

    private Integer[] pGlovesSkills;
    private Integer[] mGlovesSkills;

    private Integer weaponOffset;
    private Integer bodyOffset;
    private Integer headOffset;
    private Integer shoesOffset;
    private Integer glovesOffset;

    private Integer[] additionalSkillChance;
    private Integer[] skillLvlChances;
    private Integer[] skillsRange;
    private Integer[] magicItemsIds;
    private long[] randomStartPrices;
    private Integer destroyItemMaxSkillDivider;

    private Integer[] destroyItemCurrencyBackAmount;

    private Integer[] itemsCBLvls;

    private Integer fixedItemChanceAdd;
    private Integer fixedItemChanceAddLimit;

    private Integer fixedItemGroupChanceAdd;
    private Integer fixedItemGrpupChanceAddLimit;

    public Integer[] getBannedItemIds() {
        return bannedItemIds;
    }

    public void setBannedItemIds(Integer[] bannedItemIds) {
        this.bannedItemIds = bannedItemIds;
    }

    private Integer[] bannedItemIds;

    private Double randomPriceMul;

    public Integer[] getAdditionalSkillChance() {
        return additionalSkillChance;
    }

    public void setAdditionalSkillChance(Integer[] additionalSkillChance) {
        this.additionalSkillChance = additionalSkillChance;
    }

    public Integer[] getSkillLvlChances() {
        return skillLvlChances;
    }

    public void setSkillLvlChances(Integer[] skillLvlChances) {
        this.skillLvlChances = skillLvlChances;
    }

    public Integer[] getSkillsRange() {
        return skillsRange;
    }

    public void setSkillsRange(Integer[] skillsRange) {
        this.skillsRange = skillsRange;
    }

    public Integer[] getMagicItemsIds() {
        return magicItemsIds;
    }

    public void setMagicItemsIds(Integer[] magicItemsIds) {
        this.magicItemsIds = magicItemsIds;
    }

    public long[] getRandomStartPrices() {
        return randomStartPrices;
    }

    public void setRandomStartPrices(long[] randomStartPrices) {
        this.randomStartPrices = randomStartPrices;
    }

    public Double getRandomPriceMul() {
        return randomPriceMul;
    }

    public void setRandomPriceMul(Double randomPriceMul) {
        this.randomPriceMul = randomPriceMul;
    }

    public statsRndParams() {
    }

    public Integer[] getSkillIds() {
        return skillIds;
    }

    public void setSkillIds(Integer[] skillIds) {
        this.skillIds = skillIds;
    }

    public Integer[] getpWeapSkills() {
        return pWeapSkills;
    }

    public void setpWeapSkills(Integer[] pWeapSkills) {
        this.pWeapSkills = pWeapSkills;
    }

    public Integer[] getmWeapSkills() {
        return mWeapSkills;
    }

    public void setmWeapSkills(Integer[] mWeapSkills) {
        this.mWeapSkills = mWeapSkills;
    }

    public Integer[] getpHeadSkills() {
        return pHeadSkills;
    }

    public void setpHeadSkills(Integer[] pHeadSkills) {
        this.pHeadSkills = pHeadSkills;
    }

    public Integer[] getmHeadSkills() {
        return mHeadSkills;
    }

    public void setmHeadSkills(Integer[] mHeadSkills) {
        this.mHeadSkills = mHeadSkills;
    }

    public Integer[] getpBodySkills() {
        return pBodySkills;
    }

    public void setpBodySkills(Integer[] pBodySkills) {
        this.pBodySkills = pBodySkills;
    }

    public Integer[] getmBodySkills() {
        return mBodySkills;
    }

    public void setmBodySkills(Integer[] mBodySkills) {
        this.mBodySkills = mBodySkills;
    }

    public Integer[] getpShoesSkills() {
        return pShoesSkills;
    }

    public void setpShoesSkills(Integer[] pShoesSkills) {
        this.pShoesSkills = pShoesSkills;
    }

    public Integer[] getmShoesSkills() {
        return mShoesSkills;
    }

    public void setmShoesSkills(Integer[] mShoesSkills) {
        this.mShoesSkills = mShoesSkills;
    }

    public Integer[] getpGlovesSkills() {
        return pGlovesSkills;
    }

    public void setpGlovesSkills(Integer[] pGlovesSkills) {
        this.pGlovesSkills = pGlovesSkills;
    }

    public Integer[] getmGlovesSkills() {
        return mGlovesSkills;
    }

    public void setmGlovesSkills(Integer[] mGlovesSkills) {
        this.mGlovesSkills = mGlovesSkills;
    }

    public Integer getWeaponOffset() {
        return weaponOffset;
    }

    public void setWeaponOffset(Integer weaponOffset) {
        this.weaponOffset = weaponOffset;
    }

    public Integer getBodyOffset() {
        return bodyOffset;
    }

    public void setBodyOffset(Integer bodyOffset) {
        this.bodyOffset = bodyOffset;
    }

    public Integer getHeadOffset() {
        return headOffset;
    }

    public void setHeadOffset(Integer headOffset) {
        this.headOffset = headOffset;
    }

    public Integer getShoesOffset() {
        return shoesOffset;
    }

    public void setShoesOffset(Integer shoesOffset) {
        this.shoesOffset = shoesOffset;
    }

    public Integer getGlovesOffset() {
        return glovesOffset;
    }

    public void setGlovesOffset(Integer glovesOffset) {
        this.glovesOffset = glovesOffset;
    }

    public Integer[] getDestroyItemCurrencyBackAmount() {
        return destroyItemCurrencyBackAmount;
    }

    public void setDestroyItemCurrencyBackAmount(Integer[] destroyItemCurrencyBackAmount) {
        this.destroyItemCurrencyBackAmount = destroyItemCurrencyBackAmount;
    }

    public Integer getDestroyItemMaxSkillDivider() {
        return destroyItemMaxSkillDivider;
    }

    public void setDestroyItemMaxSkillDivider(Integer destroyItemMaxSkillDivider) {
        this.destroyItemMaxSkillDivider = destroyItemMaxSkillDivider;
    }

    public Integer getFixedItemChanceAdd() {
        return fixedItemChanceAdd;
    }

    public void setFixedItemChanceAdd(Integer fixedItemChanceAdd) {
        this.fixedItemChanceAdd = fixedItemChanceAdd;
    }

    public Integer getFixedItemChanceAddLimit() {
        return fixedItemChanceAddLimit;
    }

    public void setFixedItemChanceAddLimit(Integer fixedItemChanceAddLimit) {
        this.fixedItemChanceAddLimit = fixedItemChanceAddLimit;
    }

    public Integer getFixedItemGrpupChanceAddLimit() {
        return fixedItemGrpupChanceAddLimit;
    }

    public void setFixedItemGrpupChanceAddLimit(Integer fixedItemGrpupChanceAddLimit) {
        this.fixedItemGrpupChanceAddLimit = fixedItemGrpupChanceAddLimit;
    }

    public Integer getFixedItemGroupChanceAdd() {
        return fixedItemGroupChanceAdd;
    }

    public void setFixedItemGroupChanceAdd(Integer fixedItemGroupChanceAdd) {
        this.fixedItemGroupChanceAdd = fixedItemGroupChanceAdd;
    }

    public Integer[] getItemsCBLvls() {
        return itemsCBLvls;
    }

    public void setItemsCBLvls(Integer[] itemsCBLvls) {
        this.itemsCBLvls = itemsCBLvls;
    }
}
