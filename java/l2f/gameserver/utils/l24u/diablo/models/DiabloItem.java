package l2f.gameserver.utils.l24u.diablo.models;

import l2f.gameserver.utils.l24u.diablo.models.entity.DiabloItemSkill;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dimkk on 1/4/2017.
 */
public class DiabloItem {
    private Integer objId;
    private String name;
    private String icon;
    private long currentRandom;
    private List<DiabloItemSkill> skills;
    private Integer bodyPart;
    private boolean isMagic;
    private String grade;

    public Integer getObjId() {
        return objId;
    }

    public void setObjId(Integer objId) {
        this.objId = objId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public long getCurrentRandom() {
        return currentRandom;
    }

    public void setCurrentRandom(long currentRandom) {
        this.currentRandom = currentRandom;
    }

    public List<DiabloItemSkill> getSkills() {
        return skills;
    }

    public void setSkills(List<DiabloItemSkill> skills) {
        this.skills = skills;
    }
    public void addSkill(DiabloItemSkill skill) {
        if (this.skills == null) this.skills = new ArrayList<DiabloItemSkill>();
        this.skills.add(skill);
    }

    public DiabloItem() {

    }

    public DiabloItem(Integer objId, String name, String icon, long currentRandom, Integer bodyPart, boolean isMagic) {
        this.objId = objId;
        this.name = name;
        this.icon = icon;
        this.currentRandom = currentRandom;
        this.bodyPart = bodyPart;
        this.isMagic = isMagic;
    }

    public DiabloItem(Integer objId, String name, String icon, long currentRandom, Integer bodyPart, boolean isMagic, String grade) {
        this.objId = objId;
        this.grade = grade;
        this.name = name;
        this.icon = icon;
        this.currentRandom = currentRandom;
        this.bodyPart = bodyPart;
        this.isMagic = isMagic;
    }
}
