package l2f.gameserver.utils.l24u.diablo.models;

/**
 * Created by dimkk on 11/29/2016.
 */
public class htmlMessage {
    public htmlMessage(String title, String locale) {
        this.title = title;
        this.locale = locale;
    }

    private String title;
    private String Contents;
    private String locale;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContents() {
        return Contents;
    }

    public void setContents(String contents) {
        Contents = contents;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
