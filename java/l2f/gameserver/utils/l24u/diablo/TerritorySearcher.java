package l2f.gameserver.utils.l24u.diablo;

import l2f.commons.geometry.Polygon;
import l2f.commons.l24u.Communication.CommunicationObject;
import l2f.commons.logging.LoggerObject;
import l2f.gameserver.model.Player;
import l2f.gameserver.model.Territory;
import l2f.gameserver.utils.Location;
import l2f.gameserver.utils.l24u.diablo.models.l2Point;
import l2f.gameserver.utils.l24u.diablo.models.zone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dima on 09.11.2016.
 */
public class TerritorySearcher extends LoggerObject {

    protected static final Logger _log = LoggerFactory.getLogger("TerritorySearcher");

    // Вход
    public static Location getPoint1(){return point1;}
    // Выход
    public static Location getPoint2(){return point2;}
    public static Territory getTerritory(){return chosenTer;}

    public static RiftLoc generatePointsAndTerritory(Player player, boolean useOld) throws Exception {
        if (player.get_riftGenerator().zoneToTest != null) {
            useOld = false;
        }
        getPoints(player, useOld);
        return new RiftLoc(point1, point2, chosenTer, mobsCount);
    }
    @Deprecated
    public static RiftLoc generatePointsAndTerritory(boolean useOld) throws Exception {
        getPoints(null, useOld);
        return new RiftLoc(point1, point2, chosenTer, mobsCount);
    }

    private static Location point1;
    private static Location point2;
    private static List<Integer> Ss = new ArrayList<Integer>();
    private static List<Territory> Ters  = new ArrayList<Territory>();
    private static Territory chosenTer;
    private static Integer mobsCount;

    private static void getPoints(Player player, boolean useOld) throws Exception {
        zone z;
        if (player.get_riftGenerator().zoneToTest != null) {
            z = player.get_riftGenerator().zoneToTest;
        } else {
            CommunicationObject obj = new CommunicationObject("riftEditor", "getRandomZone", new String[]{""});
            z = new L24uDistribution(player).communicateZoneSync(obj);
        }
        if (z != null){
            Territory t = new Territory();
            Polygon poly = new Polygon();
            int maxz = 0; int minz = 0;
            for (l2Point p : z.getPoints()){
                if (p.getZ() >= maxz) maxz = p.getZ();
                if (p.getZ() <= minz) minz = p.getZ();
                poly.add(p.getX(), p.getY());
            }
            poly.setZmax(maxz).setZmin(minz);
            t.add(poly);
            chosenTer = t;
            point1 = new Location(z.getStartNpcLocation().getX(), z.getStartNpcLocation().getY(),z.getStartNpcLocation().getZ(), 0);
            point2 = new Location(z.getEndNpcLocation().getX(), z.getEndNpcLocation().getY(),z.getEndNpcLocation().getZ(), 0);
            if (z.getMobsCount() != null && z.getMobsCount() > 0) mobsCount = z.getMobsCount();
            player.get_riftGenerator().zoneToTest = null;
        }
    }

    private static int getsTer(Territory ter) {
        int XLength = ter.getXmax() - ter.getXmin();
        int YLength = ter.getYmax() - ter.getYmin();
        int ZLength = ter.getZmax() - ter.getZmin();
        return XLength * YLength * ZLength;
    }
}
