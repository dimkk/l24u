package l2f.gameserver.utils.l24u.diablo;

import l2f.commons.data.xml.AbstractDirParser;
import l2f.commons.geometry.Polygon;
import l2f.commons.time.cron.SchedulingPattern;
import l2f.gameserver.Config;
import l2f.gameserver.data.xml.holder.DoorHolder;
import l2f.gameserver.data.xml.holder.InstantZoneHolder;
import l2f.gameserver.data.xml.holder.SpawnHolder;
import l2f.gameserver.data.xml.holder.ZoneHolder;
import l2f.gameserver.geodata.GeoEngine;
import l2f.gameserver.geodata.PathFind;
import l2f.gameserver.handler.usercommands.impl.InstanceZone;
import l2f.gameserver.model.Player;
import l2f.gameserver.model.Territory;
import l2f.gameserver.model.World;
import l2f.gameserver.tables.SpawnTable;
import l2f.gameserver.templates.DoorTemplate;
import l2f.gameserver.templates.InstantZone;
import l2f.gameserver.templates.InstantZone.SpawnInfo;
import l2f.gameserver.templates.StatsSet;
import l2f.gameserver.templates.ZoneTemplate;
import l2f.gameserver.templates.spawn.SpawnTemplate;
import l2f.gameserver.utils.Location;
import l2f.gameserver.utils.l24u.Common;
import org.dom4j.Element;
import org.napile.primitive.Containers;
import org.napile.primitive.maps.IntObjectMap;
import org.napile.primitive.maps.impl.HashIntObjectMap;

import java.io.File;
import java.util.*;

/**
 * Created by dimkk on 11/8/2016.
 */

public class  DiabloInstanceGenerator
{
	private static DiabloInstanceGenerator _instance = new DiabloInstanceGenerator();

	public static DiabloInstanceGenerator getInstance()
	{
		return _instance;
	}

	public DiabloInstanceGenerator()
	{

	}

	protected InstantZone generateInstance(Player player, Location point1, Location point2, Territory territory)
	{

		int instanceId;
		String name;
		SchedulingPattern resetReuse = new SchedulingPattern("30 6 * * *"); // Сброс реюза по умолчанию в каждые сутки в 6:30
		int timelimit = -1;
		int timer = 60;
		int mapx = -1;
		int mapy = -1;
		boolean dispelBuffs = false;
		boolean onPartyDismiss = true;
		int mobId, respawn, respawnRnd, count, sharedReuseGroup = 0;
		int collapseIfEmpty = 0;
		// 0 - точечный, в каждой указанной точке; 1 - один точечный спаун в рандомной точке; 2 - локационный
		int spawnType = 0;
		SpawnInfo spawnDat = null;
		int removedItemId = 0, removedItemCount = 0, giveItemId = 0, givedItemCount = 0, requiredQuestId = 0;
		int maxChannels = 20;
		boolean removedItemNecessity = false;
		boolean setReuseUponEntry = true;
		StatsSet params = new StatsSet();

		List<SpawnInfo> spawns = new ArrayList<SpawnInfo>();
		IntObjectMap<InstantZone.DoorInfo> doors = Containers.emptyIntObjectMap();
		Map<String, InstantZone.ZoneInfo> zones = Collections.emptyMap();
		Map<String, InstantZone.SpawnInfo2> spawns2 = Collections.emptyMap();


		instanceId = Common.getRndBetween(9000, 548000);
		name = "Generated Rift id - " + Integer.toString(instanceId);

		timelimit = 30;

		collapseIfEmpty = 1;

		maxChannels = 1000;
		dispelBuffs = false;

		int minLevel = 0, maxLevel = 99, minParty = 1, maxParty = 9;
		List<Location> teleportLocs = new ArrayList<Location>(1);

		teleportLocs.add(point1);
		Location ret = null;
		onPartyDismiss = false;
		timer = 5;

		//TODO возможные улучшения
//		removedItemId = Integer.parseInt(subElement.attributeValue("itemId"));
//		removedItemCount = Integer.parseInt(subElement.attributeValue("count"));
//		removedItemNecessity = Boolean.parseBoolean(subElement.attributeValue("necessary"));

//		giveItemId = Integer.parseInt(subElement.attributeValue("itemId"));
//		givedItemCount = Integer.parseInt(subElement.attributeValue("count"));

//		requiredQuestId = Integer.parseInt(subElement.attributeValue("id"));
//
//		ret = Location.parseLoc(subElement.attributeValue("loc"));
//
//		resetReuse = new SchedulingPattern(subElement.attributeValue("resetReuse"));
//		sharedReuseGroup = Integer.parseInt(subElement.attributeValue("sharedReuseGroup"));
//		setReuseUponEntry = Boolean.parseBoolean(subElement.attributeValue("setUponEntry"));
            String[] mobs = new String[]{"54001", "54002"};
            respawn = 0;

            respawnRnd = 0;
            count = 1;

            List<Location> coords1 = new ArrayList<Location>();
            coords1.add(point1);
            List<Location> coords2 = new ArrayList<Location>();
            coords2.add(point2);
            spawnType = 0;


            mobId = Integer.parseInt(mobs[0]);
            spawnDat = new SpawnInfo(spawnType, mobId, count, respawn, respawnRnd, coords1, territory);
            spawns.add(spawnDat);
            mobId = Integer.parseInt(mobs[1]);
            spawnDat = new SpawnInfo(spawnType, mobId, count, respawn, respawnRnd, coords2, territory);
            spawns.add(spawnDat);

			// TODO Алгоритм генерации
			// 1) --Бежим по группам спауна, выбираем не специальные, выбираем рандомные (проверяем, что расстояние между точками больше параметра)
			// 2) --Рандомим из подобранных группу, забираем точки спауна этой группы мобов
            // 2) Создаем инстанс
			// 3) На противоположных концах спауним вход и выход
			// 4) Портим на вход игрока
			// 5) Спауним рандомных мобов в этих локациях
			// 6) Человек доходит до выхода
			// 7) Портим в след. локу - п.1
			InstantZone instancedZone = new InstantZone(instanceId, name, resetReuse, sharedReuseGroup, timelimit, dispelBuffs, minLevel, maxLevel, minParty, maxParty, timer, onPartyDismiss, teleportLocs, ret, mapx, mapy, doors, zones, spawns2,  spawns, collapseIfEmpty, maxChannels, removedItemId, removedItemCount, removedItemNecessity, giveItemId, givedItemCount, requiredQuestId, setReuseUponEntry, params);
			//getHolder().addInstantZone(instancedZone);
			return instancedZone;

	}
}