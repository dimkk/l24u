package l2f.gameserver.utils.l24u.diablo;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import l2f.commons.lang.ArrayUtils;
import l2f.gameserver.Config;
import l2f.gameserver.data.xml.holder.NpcHolder;
import l2f.gameserver.instancemanager.RaidBossSpawnManager;
import l2f.gameserver.model.GameObject;
import l2f.gameserver.model.Player;
import l2f.gameserver.model.reward.RewardGroup;
import l2f.gameserver.model.reward.RewardList;
import l2f.gameserver.model.reward.RewardType;
import l2f.gameserver.templates.npc.Faction;
import l2f.gameserver.templates.npc.NpcTemplate;
import l2f.gameserver.utils.l24u.Common;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;


public final class DiabloNpcGenerator
{
	private static final DiabloNpcGenerator _instance = new DiabloNpcGenerator();
	private static final Logger _log = LoggerFactory.getLogger(DiabloNpcGenerator.class);
	private String[] ignoreNpc = Config.DIABLO_MOBS_IGNORE_IDS.split(" ");

	public static DiabloNpcGenerator getInstance()
	{
		return _instance;
	}

	private DiabloNpcGenerator()
	{}

	protected void generateMob(Player _player)
	{
		NpcTemplate statsNpc = NpcHolder.getInstance().getRandomNpcByLevelsRange(_player.getRiftMobLvl(), Config.DIABLO_MOBS_LVL_RANGE_MIN, Config.DIABLO_MOBS_LVL_RANGE_MAX);
		NpcTemplate template = statsNpc;

		template = modifyTemplate(template);
		_player.get_riftGenerator().get_riftStages().get(_player.get_currentRiftStage()).get_mobSpawner().addRiftNpc(template);
	}

	private NpcTemplate modifyTemplate(NpcTemplate template) {
		// 1 - Все не агры, все не социальны, 2 - Все агры как есть - социальны как есть, 3 - Все агры как есть, социальны все, 4 - Все агры на 600 ренджа, все социальны
		Faction diabloRiftFaction = new Faction("diablo_portal_faction");
		diabloRiftFaction.setRange(400);
		int finalAgrSocType = Config.DIABLO_MOBS_AGR_SOC_RANDOM ? Common.getRndBetween(1, 4) : Config.DIABLO_MOBS_AGR_SOC_TYPE;

		if (finalAgrSocType == 1){
			template.setFaction(Faction.NONE);
			template.aggroRange = 0;
		}
		if (finalAgrSocType == 2){
		}
		if (finalAgrSocType == 3){
			template.setFaction(diabloRiftFaction);
		}
		if (finalAgrSocType == 4){
			template.setFaction(diabloRiftFaction);
			template.aggroRange = 600;
		}
		return template;
	}

	public List<NpcTemplate> filterNpc(List<NpcTemplate> result) {
		List<Integer> ids = new ArrayList<>();
		Collection<NpcTemplate> filteredCollection = Collections2.filter(result, new Predicate<NpcTemplate>() {
			@Override
			public boolean apply(NpcTemplate templ) {
				if (isRaid(templ)){
					return false;
				}
				if (templ.isBoss){
					return false;
				}
				if (templ.isRaid){
					return false;
				}
				if (templ.type == "EnergySeed" || templ.type == "ReflectionBoss" || templ.type == "FestivalMonster"){
					return false;
				}
				if (templ.rewardExp == 0){
					return false;
				}
				if (!checkForAdena(templ.getRewards())){
					return false;
				}
				List<String> ints = Arrays.asList(ignoreNpc);

				if (ints.contains(Integer.toString(templ.getNpcId()))){
					return false;
				}
				return true;
			}
		});
		List<NpcTemplate> filtered = Lists.newArrayList(filteredCollection);

		return filtered;
	}

	private boolean isRaid(NpcTemplate templ) {
		return RaidBossSpawnManager.getInstance().getAllBosses().keySet().contains(templ.getNpcId());
	}

	private boolean checkForAdena(Map<RewardType, RewardList> rew)
	{
		for(RewardList reward: rew.values()){
			for(RewardGroup rg : reward){
				if (rg.isAdena()) return true;
			}
		}
		return  false;
	}
}
