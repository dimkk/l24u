package l2f.gameserver.utils.l24u.diablo;

import l2f.gameserver.model.Territory;
import l2f.gameserver.utils.Location;

/**
 * Created by dimkk on 11/12/2016.
 */
public class RiftLoc {
        private Location p1;

        public RiftLoc(Location p1, Location p2, Territory t) {
                this.setP1(p1);
                this.setP2(p2);
                this.setT(t);
        }
        public RiftLoc(Location p1, Location p2, Territory t, Integer count) {
                this.setP1(p1);
                this.setP2(p2);
                this.setT(t);
                this.mobsCount = count;
        }

        public void setMobsCount(Integer c){
                mobsCount = c;
        }
        public Integer getMobsCount(){
                return mobsCount;
        }
        private Integer mobsCount;
        private Location p2;
        private Territory t;

        public Location getP1() {
                return p1;
        }

        public void setP1(Location p1) {
                this.p1 = p1;
        }

        public Location getP2() {
                return p2;
        }

        public void setP2(Location p2) {
                this.p2 = p2;
        }

        public Territory getT() {
                return t;
        }

        public void setT(Territory t) {
                this.t = t;
        }
}
