package l2f.gameserver.utils.l24u.diablo;

import com.google.gson.Gson;
import l2f.commons.l24u.Communication.RPCClient;
import l2f.gameserver.utils.l24u.diablo.models.htmlMessage;
import l2f.gameserver.utils.l24u.diablo.models.zone;

/**
 * Created by dimkk on 2017-03-06.
 */
public class RPCDiablo extends RPCClient {
    RPCDiablo(String queueName) throws Exception {
        super(queueName);
    }
    public String call(zone message) throws Exception {
        String json = new Gson().toJson(message);
        return sendMessage(json);
    }

    public String call(l2f.commons.l24u.Communication.CommunicationObject message) throws Exception {
        String json = new Gson().toJson(message);
        return sendMessage(json);
    }

    public String call(htmlMessage message) throws Exception {
        String json = new Gson().toJson(message);
        return sendMessage(json);
    }
    public String call(String message) throws Exception {
        return sendMessage(message);
    }
}
