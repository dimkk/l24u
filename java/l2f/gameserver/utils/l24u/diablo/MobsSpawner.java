package l2f.gameserver.utils.l24u.diablo;

import l2f.commons.util.Rnd;
import l2f.gameserver.Config;
import l2f.gameserver.data.xml.holder.NpcHolder;
import l2f.gameserver.geodata.GeoEngine;
import l2f.gameserver.geodata.PathFind;
import l2f.gameserver.instancemanager.ReflectionManager;
import l2f.gameserver.model.Player;
import l2f.gameserver.model.SimpleSpawner;
import l2f.gameserver.model.Territory;
import l2f.gameserver.model.entity.Reflection;
import l2f.gameserver.templates.InstantZone;
import l2f.gameserver.templates.npc.NpcTemplate;
import l2f.gameserver.utils.Location;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dimkk on 11/9/2016.
 */
public class MobsSpawner {

    private RiftLoc _riftLoc;
    private Reflection _inst;
    private List<SimpleSpawner> _mobsSpawn;

    private Integer _maxMobsCount = 100;
    private Integer _groupsCount = 5;
    private boolean _useCalcForMobsCount = true;

    public MobsSpawner(RiftLoc riftLoc) {
        this._riftLoc = riftLoc;
        _riftNpcs = new ArrayList<NpcTemplate>();
        _useCalcForMobsCount = true;
    }

    public MobsSpawner(RiftLoc riftLoc, int newAmount) {
        this._riftLoc = riftLoc;
        _riftNpcs = new ArrayList<NpcTemplate>();
        _maxMobsCount = newAmount;
        _useCalcForMobsCount = false;
    }

    private List<NpcTemplate> _riftNpcs;

    public void testSpawnInLocs(Reflection inst, Player player){
        _inst = inst;
        Location p1 = _riftLoc.getP1();
        Location p2 = _riftLoc.getP2();
        Territory territory = _riftLoc.getT();

        _maxMobsCount = _useCalcForMobsCount ? _calculateMobsCount(territory) : _maxMobsCount;

        if (_maxMobsCount == 0) {
            _maxMobsCount = Config.DIABLO_MOBS_MIN_SPAWN_AMOUNT;
            player.sendMessage("Площадь территории мала - " + territory.getS() + " сделали как в конфиге - DIABLO_MOBS_MIN_SPAWN_AMOUNT - " + Config.DIABLO_MOBS_MIN_SPAWN_AMOUNT);
        } else {
            player.sendMessage(_maxMobsCount + " наспаунено мобов по формуле");
            player.sendMessage("Площадь территории (" + territory.getS() + ") / Коэффициент (" + Config.DIABLO_MOBS_S_DIVIDER + ") * Множитель (" + Config.DIABLO_MOBS_MULTI + ")");
        }
//        pathLocs = new ArrayList<Location>();
//        for (Integer count = 0; count < 100; count++){
//            pathLocs.add(territory.getRandomLoc(0));
//        }

        InstantZone.SpawnInfo spawnDat = null;
        List<InstantZone.SpawnInfo> spawns = new ArrayList<InstantZone.SpawnInfo>();

        for (int i = 0; i < this._groupsCount; i++){
            // Добавит мобца к _riftNpcs
            DiabloNpcGenerator.getInstance().generateMob(player);
            List<Location> coords = new ArrayList<Location>(1);
            coords.add(new Location());
            spawnDat = new InstantZone.SpawnInfo(2, 0, _maxMobsCount / _groupsCount, 0, 0, coords, territory);
            spawns.add(spawnDat);
        }

        fillSpawns(spawns);
    }

    private int _calculateMobsCount(Territory territory){
        if (_riftLoc.getMobsCount() != null && _riftLoc.getMobsCount() > 0) {
            return _riftLoc.getMobsCount();
        }
        Integer ter = territory.getS() / Config.DIABLO_MOBS_S_DIVIDER;
        return ter * Config.DIABLO_MOBS_MULTI;
    }

    public void deleteSpawns(){
        for (SimpleSpawner s : _mobsSpawn){
            s.deleteAll();
        }
        _mobsSpawn  = new ArrayList<SimpleSpawner>();
    }

    public void fillSpawns(List<InstantZone.SpawnInfo> si)
    {
        if (_mobsSpawn == null) _mobsSpawn  = new ArrayList<SimpleSpawner>();
        if(si == null){ return; }
        for(int index = 0; index < si.size(); index++)
        {
            InstantZone.SpawnInfo s = si.get(index);
            SimpleSpawner c;
            NpcTemplate npc = _riftNpcs.get(index);
            switch(s.getSpawnType())
            {
                case 0: // точечный спаун, в каждой указанной точке
                    for(Location loc : s.getCoords())
                    {
                        c = new SimpleSpawner(npc);
                        c.setReflection(_inst);
                        c.setRespawnDelay(s.getRespawnDelay(), s.getRespawnRnd());
                        c.setAmount(s.getCount());
                        c.setLoc(loc);
                        c.doSpawn(true);
                        if(s.getRespawnDelay() == 0)
                        {
                            c.stopRespawn();
                        }
                        else
                        {
                            c.startRespawn();
                        }
                        _mobsSpawn.add(c);
                    }
                    break;
                case 1: // один точечный спаун в рандомной точке
                    c = new SimpleSpawner(npc);
                    c.setReflection(_inst);
                    c.setRespawnDelay(s.getRespawnDelay(), s.getRespawnRnd());
                    c.setAmount(1);
                    c.setLoc(s.getCoords().get(Rnd.get(s.getCoords().size())));
                    c.doSpawn(true);
                    if(s.getRespawnDelay() == 0)
                    {
                        c.stopRespawn();
                    }
                    else
                    {
                        c.startRespawn();
                    }
                    _mobsSpawn.add(c);
                    break;
                case 2: // локационный спаун
                    c = new SimpleSpawner(npc);
                    c.setReflection(_inst);
                    c.setRespawnDelay(s.getRespawnDelay(), s.getRespawnRnd());
                    c.setAmount(s.getCount());
                    c.setTerritory(s.getLoc());
                    for(int j = 0; j < s.getCount(); j++)
                    {
                        c.doSpawn(true);
                    }
                    if(s.getRespawnDelay() == 0)
                    {
                        c.stopRespawn();
                    }
                    else {
                        c.startRespawn();
                    }
                    _mobsSpawn.add(c);
            }
        }
    }

    public static List<Location> pathLocs;
    public static Location getNextLoc(){
        if (pathLocs != null){
            if (pathLocs.size() - 1 >= index) {
                index++;
            } else {
                index = 0;
            }
            return pathLocs.get(index);
        } else {
            return null;
        }
    }
    private static int index;


    public List<NpcTemplate> getRiftNpcs() {
        return _riftNpcs;
    }

    public NpcTemplate getNpcTemplate(int id){
        NpcTemplate result = null;
        for (NpcTemplate npc:_riftNpcs){
            if (npc.getNpcId() == id) result = npc;
        }
        return result;
    }

    public void setRiftNpcs(List<NpcTemplate> riftNpcs) {
        this._riftNpcs = riftNpcs;
    }

    public void addRiftNpc(NpcTemplate template){
        this._riftNpcs.add(template);
    }

    public Integer getMaxMobsCount() {
        return _maxMobsCount;
    }

    public void setMaxMobsCount(Integer maxMobsCount) {
        this._maxMobsCount = maxMobsCount;
    }

    public Integer getGroupsCount() {
        return _groupsCount;
    }

    public void setGroupsCount(Integer groupsCount) {
        this._groupsCount = groupsCount;
    }
}
