package l2f.gameserver.utils.l24u.diablo;

import l2f.gameserver.model.Player;
import l2f.gameserver.model.Territory;
import l2f.gameserver.model.entity.Reflection;
import l2f.gameserver.templates.InstantZone;
import l2f.gameserver.templates.npc.NpcTemplate;
import l2f.gameserver.utils.Location;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dimkk on 11/12/2016.
 */
public class RiftStage {

    private RiftLoc _riftLoc;
    private MobsSpawner _mobSpawner;
    private InstantZone _zone;


    public RiftStage(Location point1, Location point2, Territory territory, InstantZone zone) {
        _riftLoc = new RiftLoc(point1, point2, territory);
        _zone = zone;
        _mobSpawner = new MobsSpawner(_riftLoc);
    }

    public RiftStage(RiftLoc riftLoc, InstantZone zone) {
        _riftLoc = riftLoc;
        _zone = zone;
        _mobSpawner = new MobsSpawner(_riftLoc);
    }

    public RiftLoc getRiftLoc(){return _riftLoc;}
    public MobsSpawner get_mobSpawner(){return _mobSpawner;}
    public void ResetMobsSpawner(){
        _mobSpawner.deleteSpawns();
        _mobSpawner = null;
        _mobSpawner = new MobsSpawner(_riftLoc);
    }
    public void ResetMobsSpawnerAndSpawnAmount(int newAmount, Player player, Reflection inst){
        _mobSpawner.deleteSpawns();
        _mobSpawner = null;
        _mobSpawner = new MobsSpawner(_riftLoc, newAmount);
        _mobSpawner.testSpawnInLocs(inst, player);
    }

    public InstantZone getZone() {
        return _zone;
    }

    public void setZone(InstantZone zone) {
        this._zone = zone;
    }

}
