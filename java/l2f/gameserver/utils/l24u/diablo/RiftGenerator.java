package l2f.gameserver.utils.l24u.diablo;

import l2f.commons.l24u.Communication.CommunicationObject;
import l2f.gameserver.Config;
import l2f.gameserver.ThreadPoolManager;
import l2f.gameserver.model.Player;
import l2f.gameserver.model.World;
import l2f.gameserver.model.entity.Reflection;
import l2f.gameserver.templates.InstantZone;
import l2f.gameserver.templates.npc.NpcTemplate;
import l2f.gameserver.utils.Location;
import l2f.gameserver.utils.l24u.Common;
import l2f.gameserver.utils.l24u.diablo.models.zone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;

import static l2f.gameserver.utils.ReflectionUtils.enterReflection;

/**
 * Created by dimkk on 11/8/2016.
 */
public class RiftGenerator {
    private Player _player;
    private HashMap<Integer, RiftStage> _riftStages;
    private int _riftStagesCount;
    private static final Logger _log = LoggerFactory.getLogger(RiftGenerator.class);
    private Location _playerLocBeforeRiftEnter;
    private boolean _riftInProgress = false;
    private Reflection _currentReflection;
    public boolean useOldRiftGenerationMechanism = true;
    public zone zoneToTest;
    public HashMap<Integer, RiftStage> get_riftStages(){
        return _riftStages;
    }
    // ЧТОБЫ БЫТЬ ВКУРСЕ!! int geoIndex = GeoEngine.NextGeoIndex(instantZone.getMapX(), instantZone.getMapY(), getId());
    public RiftGenerator(Player player) {
        _player = player;
    }

    public void startRiftWithZoneId(String zoneId){
        try {
            CommunicationObject obj = new CommunicationObject("riftEditor", "getZoneById", new String[]{zoneId});
            zone z = new L24uDistribution(_player).communicateZoneSync(obj);
            zoneToTest = z;
            startRift();
        } catch (Exception e) {
            _log.error("error", e);
        }
    }

    public void startRift(){
        try{
            if (_riftStages == null) generateNewRift();
            //TODO вынести в отдельный метод старта/возобновления рифта
            // Сохраняем позицию до захода
            if (_player.get_currentRiftStage() == -1) _playerLocBeforeRiftEnter = _player.getLoc();
            // добавляем значение уровня рифта
            int riftStage = _player.get_currentRiftStage() + 1;
            // на всякий пожарный проверим, что рифт есть
            if (_riftStages.get(riftStage) == null && riftStage < _riftStagesCount) {
                generateNewRiftStage(riftStage);
            }
            // Пора закрываться - перекидываем человека
            if (riftStage >= _riftStagesCount) {
                //closeRift();
                //return;
                riftStage = 0;
            }
            // Обновляем номер этажа рифта
            _player.set_currentRiftStage(riftStage);
            //заводим в зону игрока
            Reflection inst;
            //Обнуляем прогресс рифта
            _player.getNevitSystem().setPoints(0, 0);
            inst = enterReflection(_player, new Reflection(), _riftStages.get(riftStage).getZone());
            if (_player.isInParty() && _player.isPartyLeader()) {
                for (Player p:_player.getParty()){
                    if (p.getObjectId() != _player.getObjectId()) {
                        enterReflection(p, inst, _riftStages.get(riftStage).getZone());
                        p.getNevitSystem().setPoints(0, 0);
                    }
                }
            }

            _currentReflection = inst;
            set_riftInProgress(true);
            _riftStages.get(riftStage).get_mobSpawner().testSpawnInLocs(inst, _player);
            initJobs(riftStage);



        } catch (Exception ex){
            _log.error("error", ex);
        }

    }

    private void enterPreviousRift() {
        enterReflection(_player, _currentReflection, _riftStages.get(_player.get_currentRiftStage()).getZone());
        initJobs(_player.get_currentRiftStage());
    }

    public void startRiftFinish(){
        closeRift();
    }

    private void closeRift() {
        _player.teleToLocation(_playerLocBeforeRiftEnter, 0);
        _player.setRiftGenerator(null);
        _player.set_currentRiftStage(-1);
        set_riftInProgress(false);
        _player.getNevitSystem().setPoints(0, 0);
        if (_player.isInParty() && _player.isPartyLeader()) {
            for (Player p:_player.getParty()){
                if (p.getObjectId() != _player.getObjectId()) {
                    p.teleToLocation(_playerLocBeforeRiftEnter, 0);
                }
            }
        }
        return;
    }

    private void generateNewRiftStage(int riftStage) throws Exception{
        if ((riftStage != 0)&&(riftStage >= _riftStagesCount || _riftStages.get(riftStage-1) == null)){
            _log.error("rift stage generate error: wrong stage number! for player " + _player.getAccountName());
            throw new Exception("rift stage generate error: wrong stage number!");
        }
        RiftLoc loc = TerritorySearcher.generatePointsAndTerritory(_player, useOldRiftGenerationMechanism);
        _player.sendMessage("S OF TERRITORY: " + loc.getT().getS());
        InstantZone iz = DiabloInstanceGenerator.getInstance().generateInstance(_player, loc.getP1(), loc.getP2(), loc.getT());
        _riftStages.put(riftStage, new RiftStage(loc, iz));
    }

    private void generateNewRift() {
        _riftStagesCount = Common.getRndBetween(Config.DIABLO_RIFT_MIN_STAGES, Config.DIABLO_RIFT_MAX_STAGES);
        _riftStages = new HashMap<Integer, RiftStage>(_riftStagesCount);
    }

    private void initJobs(final int stageId) {
        _player.addRadarWithMap(_riftStages.get(stageId).getRiftLoc().getP2());
        ThreadPoolManager.getInstance().scheduleAtFixedRate(new Runnable() { //Убираем
            @Override
            public void run() {
                _player.addRadarWithMap(_riftStages.get(_player.get_currentRiftStage()).getRiftLoc().getP2());
                if (checkIfMobsRespawnsNeeded()) respawnNpcsWithProperLevel();
            }

            private void respawnNpcsWithProperLevel() {
                int currentMobsAmount = World.getAroundNpc(_player).size();
                get_riftStages().get(stageId).ResetMobsSpawnerAndSpawnAmount(currentMobsAmount, _player, _currentReflection);
            }

            private boolean checkIfMobsRespawnsNeeded() {
                boolean result = false;
                List<NpcTemplate> templates = get_riftStages().get(stageId).get_mobSpawner().getRiftNpcs();
                for(NpcTemplate template : templates){
                    if (_player.getLevel() - template.level  > Config.DIABLO_MOBS_LVL_MAX_DIFF) result = true;
                    break;
                }
                return result;
            }
        }, 2000, Config.DIABLO_LOOP_ACTIONS_DELAY);
    }

    public boolean is_riftInProgress() {
        return _riftInProgress;
    }

    public void set_riftInProgress(boolean _riftInProgress) {
        this._riftInProgress = _riftInProgress;
    }
}
