package l2f.gameserver.utils.l24u.diablo;

import com.google.common.reflect.TypeToken;
import com.google.gson.GsonBuilder;
import l2f.commons.threading.RunnableImpl;
import l2f.gameserver.Config;
import l2f.gameserver.ThreadPoolManager;
import l2f.gameserver.handler.bbs.impl.CommunityDiabloItems;
import l2f.gameserver.model.GameObjectsStorage;
import l2f.gameserver.model.Player;
import l2f.gameserver.network.serverpackets.Say2;
import l2f.gameserver.network.serverpackets.ShowBoard;
import l2f.gameserver.network.serverpackets.components.ChatType;
import l2f.gameserver.scripts.Functions;
import l2f.gameserver.utils.l24u.Common;
import l2f.gameserver.utils.l24u.diablo.models.entity.zoneEditManager;
import l2f.gameserver.utils.l24u.diablo.models.statsRndParams;
import l2f.gameserver.utils.l24u.diablo.models.zone;
import l2f.loginserver.accounts.Account;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by dimkk on 11/29/2016.
 */
public class L24uDistribution extends Functions {
    protected static final Logger _log = LoggerFactory.getLogger("L24uDistribution");

    private Player p;

    public L24uDistribution(Player player) {
        p = player;
    }


    public static void parseAndRouteMessage(String message){
        try {
            l2f.commons.l24u.Communication.CommunicationObject obj = l2f.commons.l24u.Communication.l24uDistrShared.getCommunicationObjectFromString(message);
            if (obj == null) return;
            if (obj.getSection().equalsIgnoreCase("chat")) {
                // TODO виды чатов
                String name = obj.getHashVal("name");
                String to = obj.getHashVal("to");
                String say = obj.getHashVal("message");
                Say2 cs = new Say2(0, ChatType.SHOUT, name, say);
                for (Player player : GameObjectsStorage.getAllPlayersForIterate())
                    if (!player.isBlockAll())
                        player.sendPacket(cs);
            }
            if (obj.getSection().equalsIgnoreCase("lk")) {
                if (obj.getMethod().equalsIgnoreCase("createAccount")) {
                    String login = obj.getHashVal("login");
                    String password = obj.getHashVal("password");
                    String email = obj.getHashVal("email");
                    String actor = obj.getHashVal("actor");
                    _log.info(String.format("%s created account %s, with site [uid:login:email] [%s]", actor, login, email));
                    // regex что-то не пашет, надо разбираться
                    //if ((login.matches(l2f.loginserver.Config.ANAME_TEMPLATE)) && (password.matches(l2f.loginserver.Config.APASSWD_TEMPLATE))) {
                    Account account = new Account(login);
                    account.restore();
                    account.setAllowedIP("");
                    account.setAllowedHwid("");
                    account.setPasswordHash(password);
                    account.setEmail(email);
                    try {
                        account.save();
                    } catch (Exception ex) {
                        _log.error("err", ex);
                        if (actor.equalsIgnoreCase("admin") || actor.equalsIgnoreCase("superadmin") || email.equalsIgnoreCase(account.getEmail())) {
                            account.setEmail(email);
                            account.setPasswordHash(password);
                            account.update();
                        }
                    }
                    //}

                }
                if (obj.getMethod().equalsIgnoreCase("deleteAccount")) {
                    String login = obj.getHashVal("login");
                    String actor = obj.getHashVal("actor");
                    _log.info(String.format("%s deleted account %s, with site", actor, login));
                    Account account = new Account(login);
                    account.restore();
                    account.setPasswordHash("randomPassl24u" + Common.getRndBetween(500, 500000));
                    account.update();

                }
            }
        } catch (Exception ex) {
            _log.warn("communication error", ex);
        }
    }

    public  static void publishChatMessage(String message, String name) {
        l2f.commons.l24u.Communication.CommunicationObject obj = new l2f.commons.l24u.Communication.CommunicationObject("chat", "ingame");
        obj.addHash("name", name);
        obj.addHash("message", message);
        l2f.commons.l24u.Communication.PublishMessage.call(obj, Config.MQ_QUEUE_NAME+"chatout");
    }

    // Тут получаем ид скилов и их распределения для рандома скилов итемов
    public static statsRndParams getDistributedStatsRndParamsSync() {
        l2f.commons.l24u.Communication.CommunicationObject obj = new l2f.commons.l24u.Communication.CommunicationObject("statsRnd", "getStatsRndParams");
        String response = getResponseSync(obj);
        statsRndParams result = parseStatsRndParams(response);
        return result;
    }

    // Todo сделать загрузку по возможности асинхронной и сделать TTL рабочий
//    public static statsRndParams getSetDistributedStatsRndParamsASync() {
//        ThreadPoolManager.getInstance().schedule(new RunnableImpl() {
//            @Override
//            public void runImpl() throws Exception {
//                CommunicationObject obj = new CommunicationObject("statsRnd", "getStatsRndParams");
//                String response = getResponseSync(obj);
//                statsRndParams result = parseStatsRndParams(response);
//            }
//        }, 0);
//    }

    // Тут получаем всякие xml данные
    // dataType - skills, items
    public static Collection<File> getDistributedDataSync(String dataType) {
        Collection<File> result = new ArrayList<>();
        l2f.commons.l24u.Communication.CommunicationObject obj = new l2f.commons.l24u.Communication.CommunicationObject("statsRnd", "getData", new String[]{dataType});
        String response = getResponseSync(obj);
        result.addAll(parseDataResponse(response, dataType));
        return result;
    }

    // Общая точка входа в коммуникации
    public void communicate(l2f.commons.l24u.Communication.CommunicationObject object){
        try {
            if (object.getSection().equals("riftEditor")){
                object.addHash("playerId", Integer.toString(p.getObjectId()));
                object.addHash("coords", p.getLoc().toString());
                sendRiftZoneCommObject(object);
            }
            if (object.getSection().equals("statsRnd")){
                object = CommunityDiabloItems.prepareCommunication(object, p);
                sendStatsRndCommObject(object);
            }
        } catch (Exception e) {
            _log.error("errrr", e);
        } finally {
            return;
        }
    }

    // Получаем зону для рифта
    public zone communicateZoneSync(l2f.commons.l24u.Communication.CommunicationObject object){
        zone z = null;
        try {
            if (object.getSection().equals("riftEditor")){
                GsonBuilder gson = new GsonBuilder();
                z = gson.create().fromJson(new RPCDiablo(Config.MQ_QUEUE_NAME+object.getSection()).call(object), zone.class);
            }
        } catch (Exception e) {
            _log.error("errrr", e);
        }
        return z;
    }

    private void sendStatsRndCommObject(l2f.commons.l24u.Communication.CommunicationObject obj) throws Exception {
        ThreadPoolManager.getInstance().schedule(new RunnableImpl() {
            @Override
            public void runImpl() throws Exception {
                String response = new RPCDiablo(Config.MQ_QUEUE_NAME+obj.getSection()).call(obj);
                ShowBoard.separateAndSend(response, p);
            }
        }, 0);
    }

    private void sendRiftZoneCommObject(l2f.commons.l24u.Communication.CommunicationObject obj) throws Exception {
        ThreadPoolManager.getInstance().schedule(new RunnableImpl() {
            @Override
            public void runImpl() throws Exception {
                if (obj.getMethod().equals("goIntoRift")){
                    p.get_riftGenerator().startRiftWithZoneId(obj.getArgs()[0]);
                    return;
                }
                String response = new RPCDiablo(Config.MQ_QUEUE_NAME + obj.getSection()).call(obj);
                if (obj.getMethod().equals("teleToPoint")){
                    String rawZone = response;
                    zone z = new GsonBuilder().create().fromJson(rawZone, zone.class);
                    zoneEditManager.getInstance().moveToPoint(p, z, obj.getArgs());
                }
                show(response, p);
            }
        }, 0);
    }

    private static Collection<File> parseDataResponse(String response, String dataType) {
        Collection<File> result = new ArrayList<>();
        List<String> respFiles = new ArrayList<>();
        GsonBuilder gson = new GsonBuilder();
        Type listType = new TypeToken<ArrayList<String>>() {
        }.getType();
        respFiles = gson.create().fromJson(response, listType);
        int i = 0;
        for (String fileContents:respFiles){
            File f = new File(dataType + "-" + i + ".xml");
            try {
                FileUtils.writeStringToFile(f, fileContents);
            } catch (IOException e) {
                _log.error("errrr", e);
            }
            result.add(f);
        }
        return result;
    }

    private static String getResponseSync(l2f.commons.l24u.Communication.CommunicationObject obj) {
        String result = "";
        try {
            result = new RPCDiablo(Config.MQ_QUEUE_NAME+obj.getSection()).call(obj);

        } catch (Exception e) {
            _log.error("errrr", e);
        }
        return result;
    }

    private static statsRndParams parseStatsRndParams(String response) {
        GsonBuilder gson = new GsonBuilder();
        statsRndParams result = gson.create().fromJson(response, statsRndParams.class);
        return result;
    }

}

