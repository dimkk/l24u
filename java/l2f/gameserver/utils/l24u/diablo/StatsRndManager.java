package l2f.gameserver.utils.l24u.diablo;

import com.google.common.base.Strings;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import l2f.commons.collections.CollectionUtils;
import l2f.gameserver.Config;
import l2f.gameserver.dao.ItemsDAO;
import l2f.gameserver.dao.diabloDAO;
import l2f.gameserver.handler.bbs.impl.CommunityDiabloItems;
import l2f.gameserver.model.Player;
import l2f.gameserver.model.Skill;
import l2f.gameserver.model.World;
import l2f.gameserver.model.items.ItemInstance;
import l2f.gameserver.network.GameClient;
import l2f.gameserver.network.loginservercon.AuthServerCommunication;
import l2f.gameserver.skills.SkillsEngine;
import l2f.gameserver.tables.SkillTable;
import l2f.gameserver.templates.item.ItemTemplate;
import l2f.gameserver.utils.Log;
import l2f.gameserver.utils.l24u.Common;
import l2f.gameserver.utils.l24u.diablo.models.DiabloItem;
import l2f.gameserver.utils.l24u.diablo.models.DiabloItemsObj;
import l2f.gameserver.utils.l24u.diablo.models.entity.DiabloItemSkill;
import l2f.gameserver.utils.l24u.diablo.models.statsRndParams;
import l2f.loginserver.AuthServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.crypto.dsig.keyinfo.KeyValue;
import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by dimkk on 12/26/2016.
 */
public class StatsRndManager {
    private static final String VarForItems = "diabloItems";
    public static final String levelsSplit = ":";
    private static final Logger _log = LoggerFactory.getLogger(StatsRndManager.class);

    public static void setParamsByDistribution(statsRndParams params){
        skillIds = params.getSkillIds();

        pWeapSkills = params.getpWeapSkills();
        mWeapSkills = params.getmWeapSkills();

        pHeadSkills = params.getpHeadSkills();
        mHeadSkills = params.getmHeadSkills();

        pBodySkills = params.getpBodySkills();
        mBodySkills = params.getmBodySkills();

        pShoesSkills = params.getpShoesSkills();
        mShoesSkills = params.getmShoesSkills();

        pGlovesSkills = params.getpGlovesSkills();
        mGlovesSkills = params.getmGlovesSkills();

        weaponOffset = params.getWeaponOffset();
        bodyOffset = params.getBodyOffset();
        headOffset = params.getHeadOffset();
        shoesOffset = params.getShoesOffset();
        glovesOffset = params.getGlovesOffset();

        additionalSkillChance = params.getAdditionalSkillChance();
        skillLvlChances = params.getSkillLvlChances();
        skillsRange = params.getSkillsRange();
        magicItemsIds = params.getMagicItemsIds();
        randomStartPrices = params.getRandomStartPrices();
        randomPriceMul = params.getRandomPriceMul();
        destroyItemCurrencyBackAmount = params.getDestroyItemCurrencyBackAmount();
        destroyItemMaxSkillDivider = params.getDestroyItemMaxSkillDivider();
        itemsCBLvls = params.getItemsCBLvls();
        if (params.getBannedItemIds() != null) {
            bannedItemIds = params.getBannedItemIds();
        }

        if (params.getFixedItemChanceAdd() != null) {
            setGetFixedItemChanceAdd(params.getFixedItemChanceAdd() * 10000);
        }

        if (params.getFixedItemChanceAddLimit() != null) {
            setGetFixedItemChanceAddLimit(params.getFixedItemChanceAddLimit() * 10000);
        }

        if (params.getFixedItemGroupChanceAdd() != null) {
            setGetFixedItemGroupChanceAdd(params.getFixedItemGroupChanceAdd() * 10000);
        }

        if (params.getFixedItemGrpupChanceAddLimit() != null) {
            setGetFixedItemGroupChanceAddLimit(params.getFixedItemGrpupChanceAddLimit() * 10000);
        }

        _log.info("distributed statsRndParams loaded!");
    }

    public static List<Integer> getSkillIds(){
        return Arrays.asList(skillIds);
    }

    private static Integer[] skillIds = new Integer[]{
          //patt     pdef     matt     mdef     critCh
            90100,   90105,   90110,   90115,   90120,
          //mCritCh  critDmg  mCritDmg attSp    castSpeed
            90125,   90130,   90135,   90140,   90145,
          //run
            90150
    };

    //патака(матака), пдеф, мдеф, критшанс(мкритшанс), критдмг(мкритдмг), ск. атаки(каста), бег
    private static Integer[] pWeapSkills = new Integer[] {90100, 90105, 90115, 90120, 90130, 90140, 90150};
    private static Integer[] mWeapSkills = new Integer[] {90110, 90105, 90115, 90125, 90135, 90145, 90150};

    //пдеф, мдеф, критшанс(мкритшанс)
    private static Integer[] pHeadSkills = new Integer[] {90105, 90115, 90120};
    private static Integer[] mHeadSkills = new Integer[] {90105, 90115, 90125};

    //пдеф, мдеф, ск. атаки(каста), бег
    private static Integer[] pBodySkills = new Integer[] {90105, 90115, 90140, 90150};
    private static Integer[] mBodySkills = new Integer[] {90105, 90115, 90145, 90150};

    //пдеф, мдеф, бег
    private static Integer[] pShoesSkills = new Integer[] {90105, 90115, 90150};
    private static Integer[] mShoesSkills = new Integer[] {90105, 90115, 90150};

    //патака(матака), пдеф, мдеф, критшанс(мкритшанс), критдмг(мкритдмг), ск. атаки(каста)
    private static Integer[] pGlovesSkills = new Integer[] {90100, 90105, 90115, 90120, 90130, 90140, 90150};
    private static Integer[] mGlovesSkills = new Integer[] {90110, 90105, 90115, 90125, 90135, 90145, 90150};

    private static Integer weaponOffset = 0;
    private static Integer bodyOffset = 2;
    private static Integer headOffset = 1;
    private static Integer shoesOffset = 3;
    private static Integer glovesOffset = 4;

    private static Integer[] additionalSkillChance = new Integer[] {100, 1000, 10000, 3000, 1, 0, 0, 0, 50, 50, 150, 200, 250};
    private static Integer[] skillLvlChances = new Integer[] {50, 100, 500, 1000, 10000};
    private static Integer[] skillsRange = new Integer[] {90100, 90500};
    public static Integer[] magicItemsIds = new Integer[] {425 , 426 , 1100 , 428 , 429 , 1101 , 1102 , 1312 , 1310 , 12010 , 432 , 433 , 435 , 434 , 437 , 2396 , 16880 , 439 , 441 , 442 , 2397 , 2398 , 2399 , 5304 , 2400 , 2423 , 2433 , 2430 , 2435 , 5732 , 5728 , 5736 , 5740 , 5779 , 5782 , 5788 , 5785 , 6385 , 9440 , 16303 , 15620 , 13446 , 16320 , 15603 , 15586 , 427 , 430 , 431 , 438 , 440 , 2406 , 2407 , 2409 , 2408 , 6383 , 7858 , 2447 , 2451 , 2454 , 2459 , 2463 , 5715 , 5712 , 5720 , 5724 , 5767 , 5770 , 5771 , 5776 , 6384 , 9439 , 15617 , 16300 , 16317 , 13445 , 13455 , 16848 , 15600 , 15583 , 41 , 47 , 1149 , 529 , 537 , 499 , 2415 , 503 , 2416 , 2417 , 512 , 547 , 2418 , 2419 , 6386 , 9438 , 15608 , 15606 , 13139 , 16308 , 15591 , 15574};
    private static long[] randomStartPrices = new long[] {100, 1000, 10000, 100000, 500000, 1000000, 10000000, 15000000};
    private static Double randomPriceMul = 1.5;
    private static Integer destroyItemMaxSkillDivider = 2;
    private static Integer[] legitBodyParts = new Integer[] {64, 1024, 32768, 512, 4096};

    public static Integer[] getBannedItemIds() {
        return bannedItemIds;
    }
    private static Integer[] itemsCBLvls = new Integer[] {76, 76, 76};
    private static Integer[] bannedItemIds = new Integer[] {8689, 8190};
    //                                                                      id,  s80, s84,s, a  b  c  d
    private static Integer[] destroyItemCurrencyBackAmount = new Integer[] {6673, 6,   7, 5, 4, 3, 2, 1};

    private static Integer getFixedItemChanceAdd = 10 * 10000;
    private static Integer getFixedItemChanceAddLimit = 10 * 10000;

    private static Integer getFixedItemGroupChanceAdd = 100 * 10000;
    private static Integer getFixedItemGroupChanceAddLimit = 1 * 10000;

    private static Integer maxSkillsInItem = 5;

    @Deprecated
    public static List<DiabloItemSkill> generateRandomSkillsForItem(Integer itemObjId, Integer skillToChangeId, Integer rndSkillAmount) {
        List<DiabloItemSkill> rndSkills = new ArrayList<>();
        ItemInstance item = ItemsDAO.getInstance().load(itemObjId);
        // уберем скилл который выбрали
        // пока нет понимания необходимости убрать этот скилл
        //List<Skill> cleanSkills = item.getItemInstanceSkills().stream().filter(s -> s.getId() != skillToChangeId).collect(Collectors.toList());
        //item.setItemInstanceSkills(cleanSkills, false);
        for (int i=0; i < rndSkillAmount; i++){
            Skill s = null;
            Integer skillLvl = getSkillLvl();
            Integer skillId = getSkillId(item.getTemplate(), item, skillLvl, skillToChangeId);
            Skill resultSkill = SkillTable.getInstance().getInfo(skillId, skillLvl);
            if (resultSkill != null)
                rndSkills.add(DiabloItemSkill.parse(resultSkill));
        }
        //List<DiabloItemsObj> itemSkills = diabloDAO.getDiabloItemObjectForItem(itemObjId);
        return rndSkills;
    }

    public static Integer restoreSkillsAndGetCount(ItemInstance item) {
        List<DiabloItemsObj> items = diabloDAO.getDiabloItemObjectForItem(item.getObjectId());
        Integer skillCount = 0;
        for (DiabloItemsObj diabloItem : items){
           //Нашли нужный итем
           if (diabloItem.itemId == item.getObjectId()) {
               skillCount++;
               //Делим на скилы
               String[] skillLvl = diabloItem.skillsAndLevels.split(levelsSplit);
               Skill sk = SkillTable.getInstance().getInfo(Integer.parseInt(skillLvl[0]), Integer.parseInt(skillLvl[1]));
               if (checkSkill(sk, item)) {
                   item.addSkillToInstance(sk);
               }
               else {
                   diabloDAO.delete(item.getObjectId());
               }
           }
        }
        if (items.size() > 0)
            item.setRandomCount(items.get(0).getRandomCount());
        else
            item.setRandomCount(0);
        return skillCount;
    }

    private static boolean checkSkill(Skill sk, ItemInstance item) {
        Integer skillId = sk.getId();
        Integer wholePart = Math.round(skillId / 5);
        Integer offset = skillId - (wholePart * 5);
        return offset.equals(getOffsetForItem(item));
    }

    private static Integer getOffsetForItem(ItemInstance item) {
        ItemTemplate template = item.getTemplate();
        if (template.isWeapon()) {
            return weaponOffset;
        }
        if (template.isArmor()) {
            if (template.getBodyPart() == 64) {
                return headOffset;
            }
            if (template.getBodyPart() == 1024 || template.getBodyPart() == 32768) {
                return bodyOffset;
            }
            if (template.getBodyPart() == 512) {
                return glovesOffset;
            }
            if (template.getBodyPart() == 4096) {
                return shoesOffset;
            }
        }
        return -1;
    }

    public static void saveItemInstanceSkills(ItemInstance item) {

        List<DiabloItemsObj> items = diabloDAO.getDiabloItemObjectForItem(item.getObjectId());
        boolean found = false;
        Integer foundIdx = -1;
        if (items.size() > 0)
            items.removeAll(items);
        for (Skill s: item.getItemInstanceSkills()) {
            DiabloItemsObj newD = new DiabloItemsObj();
            newD.skillsAndLevels = s.getId() + levelsSplit + s.getLevel();
            newD.itemId = item.getObjectId();
            newD.setRandomCount(item.getRandomCount());
            items.add(newD);
        }

        diabloDAO.setJsonForList(item.getObjectId(), items);
    }

    public static void addSkillsForItem(ItemTemplate template, ItemInstance itemInstance) {
        if (template.isWeapon() || template.isArmor()) {
            Integer skills = restoreSkills(itemInstance);
            if (skills == 0) {
                // Получаем кол-во скилов для добавления
                skills = initSkillsCount(itemInstance);
            }
            for (Integer i = 0; i < skills; i++){
                addSkillForItem(template, itemInstance);
            }
        }
        Log.LogStatsRndSkills("AddSkills", itemInstance.getPlayer(), StatsRndManager.class.getClass().getSimpleName(), itemInstance);
    }

    private static Integer initSkillsCount(ItemInstance item) {
        Integer skillCount = 0;


        // 1 скила - 100%
        if (skillCount == 0) {
            skillCount++;
        }
        // 2 скила - 30%
        if (skillCount == 1) {
            if (Common.getRndBetween(0, additionalSkillChance[2]) <= additionalSkillChance[3] + getGradeAddon(item))
                skillCount++;
        }
        // 3 - 10%
        if (skillCount == 2 && (item.getTemplate().getItemGrade() == ItemTemplate.Grade.S80
                || item.getTemplate().getItemGrade() == ItemTemplate.Grade.S84
                || item.getTemplate().getItemGrade() == ItemTemplate.Grade.S)) {
            if(Common.getRndBetween(0, additionalSkillChance[2]) <= additionalSkillChance[1] + getGradeAddon(item))
                skillCount++;
        }

        // 4 - 1%
        if (skillCount == 3 && (item.getTemplate().getItemGrade() == ItemTemplate.Grade.S80 || item.getTemplate().getItemGrade() == ItemTemplate.Grade.S84)) {
            if(Common.getRndBetween(0, additionalSkillChance[2]) <= additionalSkillChance[0] + getGradeAddon(item))
                skillCount++;
        }

        // 5 - 0,01%
        if (skillCount == 3 && item.getTemplate().getItemGrade() == ItemTemplate.Grade.S84) {
            if(Common.getRndBetween(0, additionalSkillChance[2]) <= additionalSkillChance[4])
                skillCount++;
        }

        return skillCount;
    }

    private static void addSkillForItem(ItemTemplate template, ItemInstance item) {
                Skill s = null;
                Integer skillLvl = getSkillLvl();
                Integer skillId = getSkillId(template, item, skillLvl, -1);
                if (skillId != 0) {
                    item.addSkillToInstance(SkillTable.getInstance().getInfo(skillId, skillLvl));
                }
    }

    private static Integer restoreSkills(ItemInstance item){
        Integer skillCount = 0;
        if (item.getItemInstanceSkills().size() == 0) {
            skillCount = restoreSkillsAndGetCount(item);
        } else {
            for (Skill itemSkill : item.getItemInstanceSkills()) {
                if (isDiabloItemSkill(itemSkill.getId())) skillCount++;
            }
        }
        return skillCount;
    }

    private static boolean calculateAddSkill(ItemInstance item) {
        Integer skillCount = 0;


        // 1 скила - 100%
        if (skillCount == 0) {
            return true;
        }
        // 2 скила - 30%
        if (skillCount == 1) {
            return Common.getRndBetween(0, additionalSkillChance[2]) <= additionalSkillChance[3] + getGradeAddon(item);
        }
        // 3 - 10%
        if (skillCount == 2 && (item.getTemplate().getItemGrade() == ItemTemplate.Grade.S80
                || item.getTemplate().getItemGrade() == ItemTemplate.Grade.S84
                || item.getTemplate().getItemGrade() == ItemTemplate.Grade.S)) {
            return Common.getRndBetween(0, additionalSkillChance[2]) <= additionalSkillChance[1] + getGradeAddon(item);
        }

        // 4 - 1%
        if (skillCount == 3 && (item.getTemplate().getItemGrade() == ItemTemplate.Grade.S80 || item.getTemplate().getItemGrade() == ItemTemplate.Grade.S84)) {
            return Common.getRndBetween(0, additionalSkillChance[2]) <= additionalSkillChance[0] + getGradeAddon(item);
        }

        // 5 - 0,01%
        if (skillCount == 3 && item.getTemplate().getItemGrade() == ItemTemplate.Grade.S84) {
            return Common.getRndBetween(0, additionalSkillChance[2]) <= additionalSkillChance[4] + getGradeAddon(item);
        }

        return false;
    }




    // Рандомим 0 - 10 000, если попадаем в нужный интервал - сработал шанс
    private static Integer getSkillLvl() {
        Boolean topLvl = Common.getRndBetween(0, skillLvlChances[4]) <= skillLvlChances[0];
        if (topLvl) return Common.getRndBetween(41, 51);

        Boolean highLvl = Common.getRndBetween(0, skillLvlChances[4]) <= skillLvlChances[1];
        if (highLvl) return Common.getRndBetween(31, 41);

        Boolean midLvl = Common.getRndBetween(0, skillLvlChances[4]) <= skillLvlChances[2];
        if (midLvl) return Common.getRndBetween(21, 31);

        Boolean lowLvl = Common.getRndBetween(0, skillLvlChances[4]) <= skillLvlChances[3];
        if (lowLvl) return Common.getRndBetween(11, 21);

        return Common.getRndBetween(1, 11);
    }

    // Надо добавить скилл - ищем тот, которого нет
    private static Integer getSkillId(ItemTemplate template, ItemInstance item, Integer lvl, Integer skillToChange) {
        Integer skillId = 0;
        List<Integer> currentSkillsForItem = Collections.EMPTY_LIST;

        if (item.getItemInstanceSkills() != null)
            currentSkillsForItem = Arrays.asList(item.getItemInstanceSkills()
                    .stream()
                    .filter(s -> s.getId() != skillToChange)
                    .map(s -> s.getId())
                    .toArray(Integer[]::new));

        skillId =  getInteger(template, lvl, currentSkillsForItem);
        return skillId;
    }

    public static boolean isDiabloItemSkill(Integer skillId) {
        return skillId >= skillsRange[0] || skillId < skillsRange[1];
    }

    private static Integer getInteger(ItemTemplate template, Integer lvl, List<Integer> currentSkillIds) {
        Integer baseSkillId = 0;
        Integer rndSkillIdx = 0;
        Skill rndedSkill = null;

        //Weap
        Integer[] mWeapSubs = Arrays.stream(mWeapSkills)
                    .filter(item -> !currentSkillIds.contains(item))
                    .toArray(Integer[]::new);

        Integer[] pWeapSubs = Arrays.stream(pWeapSkills)
                .filter(item -> !currentSkillIds.contains(item))
                .toArray(Integer[]::new);

        //Head
        Integer[] mHeadSubs = Arrays.stream(mHeadSkills)
                .filter(item -> !currentSkillIds.contains(item + headOffset))
                .toArray(Integer[]::new);

        Integer[] pHeadSubs = Arrays.stream(pHeadSkills)
                .filter(item -> !currentSkillIds.contains(item + headOffset))
                .toArray(Integer[]::new);

        //Body
        Integer[] mBodySubs = Arrays.stream(mBodySkills)
                .filter(item -> !currentSkillIds.contains(item + bodyOffset))
                .toArray(Integer[]::new);

        Integer[] pBodySubs = Arrays.stream(pBodySkills)
                .filter(item -> !currentSkillIds.contains(item + bodyOffset))
                .toArray(Integer[]::new);

        //Gloves
        Integer[] mGloveSubs = Arrays.stream(mGlovesSkills)
                .filter(item -> !currentSkillIds.contains(item + glovesOffset))
                .toArray(Integer[]::new);

        Integer[] pGloveSubs = Arrays.stream(pGlovesSkills)
                .filter(item -> !currentSkillIds.contains(item + glovesOffset))
                .toArray(Integer[]::new);

        //Shoes
        Integer[] mShoesSubs = Arrays.stream(mShoesSkills)
                .filter(item -> !currentSkillIds.contains(item + shoesOffset))
                .toArray(Integer[]::new);

        Integer[] pShoesSubs = Arrays.stream(pShoesSkills)
                .filter(item -> !currentSkillIds.contains(item + shoesOffset))
                .toArray(Integer[]::new);

        try {


        if (template.isWeapon()){
            if (template.isMagicItem()) {
                rndSkillIdx = Common.getRndBetween(0, mWeapSubs.length);
                baseSkillId = mWeapSubs[rndSkillIdx];
                rndedSkill = SkillTable.getInstance().getInfo(baseSkillId + weaponOffset, lvl);
            } else {
                rndSkillIdx = Common.getRndBetween(0, pWeapSubs.length);
                baseSkillId = pWeapSubs[rndSkillIdx];
                rndedSkill = SkillTable.getInstance().getInfo(baseSkillId + weaponOffset, lvl);
            }

            return rndedSkill.getId();
        }
        if (template.isArmor()) {
            if (template.getBodyPart() == 64){
                if (template.isMagicItem()) {
                    rndSkillIdx = Common.getRndBetween(0, mHeadSubs.length);
                    baseSkillId = mHeadSubs[rndSkillIdx];
                    rndedSkill = SkillTable.getInstance().getInfo(baseSkillId + headOffset, lvl);
                } else {
                    rndSkillIdx = Common.getRndBetween(0, pHeadSubs.length);
                    baseSkillId = pHeadSubs[rndSkillIdx];
                    rndedSkill = SkillTable.getInstance().getInfo(baseSkillId + headOffset, lvl);
                }
                return rndedSkill.getId();
            }
            if (template.getBodyPart() == 1024 || template.getBodyPart() == 32768){
                if (template.isMagicItem()) {
                    rndSkillIdx = Common.getRndBetween(0, mBodySubs.length);
                    baseSkillId = mBodySubs[rndSkillIdx];
                    rndedSkill = SkillTable.getInstance().getInfo(baseSkillId + bodyOffset, lvl);
                } else {
                    rndSkillIdx = Common.getRndBetween(0, pBodySubs.length);
                    baseSkillId = pBodySubs[rndSkillIdx];
                    rndedSkill = SkillTable.getInstance().getInfo(baseSkillId + bodyOffset, lvl);
                }
                return rndedSkill.getId();
            }
            if (template.getBodyPart() == 512){
                if (template.isMagicItem()) {
                    rndSkillIdx = Common.getRndBetween(0, mGloveSubs.length);
                    baseSkillId = mGloveSubs[rndSkillIdx];
                    rndedSkill = SkillTable.getInstance().getInfo(baseSkillId + glovesOffset, lvl);
                } else {
                    rndSkillIdx = Common.getRndBetween(0, pGloveSubs.length);
                    baseSkillId = pGloveSubs[rndSkillIdx];
                    rndedSkill = SkillTable.getInstance().getInfo(baseSkillId + glovesOffset, lvl);
                }
                return rndedSkill.getId();
            }
            if (template.getBodyPart() == 4096){
                if (template.isMagicItem()) {
                    rndSkillIdx = Common.getRndBetween(0, mShoesSubs.length);
                    baseSkillId = mShoesSubs[rndSkillIdx];
                    rndedSkill = SkillTable.getInstance().getInfo(baseSkillId + shoesOffset, lvl);
                } else {
                    rndSkillIdx = Common.getRndBetween(0, pShoesSubs.length);
                    baseSkillId = pShoesSubs[rndSkillIdx];
                    rndedSkill = SkillTable.getInstance().getInfo(baseSkillId + shoesOffset, lvl);
                }
                return rndedSkill.getId();
            }
        }
        } catch (Exception ex){
            _log.error("statsRndManager get skill id error", ex);
        }
        return 0;
    }


    public static String getItemsToSend(Player player) {
        ItemInstance[] playerItems = player.getInventory().getItems();
        List<DiabloItem> dItems = new ArrayList<>();
        for (ItemInstance item: playerItems) {
            if (item.isWeapon() || item.isArmor()) { //getCurrentPrice(item)
                DiabloItem dItem = new DiabloItem(item.getObjectId(), item.getName(), item.getTemplate().getIcon(), item.getRandomCount(), item.getBodyPart(), item.getTemplate().isMagicItem(), item.getTemplate().getItemGrade().name());
                for (Skill s: item.getItemInstanceSkills()) {
                    DiabloItemSkill dSkill = new DiabloItemSkill(s.getName(), s.getLevel(), s.getIcon(), s.getId());
                    dItem.addSkill(dSkill);
                }
                dItems.add(dItem);
            }
        }
        Gson gson = new GsonBuilder().create();
        Type listType = new TypeToken<ArrayList<DiabloItem>>() {
        }.getType();
        return gson.toJson(dItems, listType);
    }

    public static String getItemsSortedByIdToSend(Player player) {
        ItemInstance[] playerItems = player.getInventory().getItemsSortById();
        List<DiabloItem> dItems = new ArrayList<>();
        for (ItemInstance item: playerItems) {
            if (item.isWeapon() || item.isArmor()) { //getCurrentPrice(item)
                DiabloItem dItem = new DiabloItem(item.getObjectId(), item.getName(), item.getTemplate().getIcon(), item.getRandomCount(), item.getBodyPart(), item.getTemplate().isMagicItem(), item.getTemplate().getItemGrade().name());
                for (Skill s: item.getItemInstanceSkills()) {
                    DiabloItemSkill dSkill = new DiabloItemSkill(s.getName(), s.getLevel(), s.getIcon(), s.getId());
                    dItem.addSkill(dSkill);
                }
                dItems.add(dItem);
            }
        }
        Gson gson = new GsonBuilder().create();
        Type listType = new TypeToken<ArrayList<DiabloItem>>() {
        }.getType();
        return gson.toJson(dItems, listType);
    }

    private static Integer getGradeAddon(ItemInstance item) {
        if (item.getTemplate().getItemGrade() == ItemTemplate.Grade.NONE) {
           return additionalSkillChance[5];
        }
        if (item.getTemplate().getItemGrade() == ItemTemplate.Grade.D) {
            return additionalSkillChance[6];
        }
        if (item.getTemplate().getItemGrade() == ItemTemplate.Grade.C) {
            return additionalSkillChance[7];
        }
        if (item.getTemplate().getItemGrade() == ItemTemplate.Grade.B) {
            return additionalSkillChance[8];
        }
        if (item.getTemplate().getItemGrade() == ItemTemplate.Grade.A) {
            return additionalSkillChance[9];
        }
        if (item.getTemplate().getItemGrade() == ItemTemplate.Grade.S) {
            return additionalSkillChance[10];
        }
        if (item.getTemplate().getItemGrade() == ItemTemplate.Grade.S80) {
            return additionalSkillChance[11];
        }
        if (item.getTemplate().getItemGrade() == ItemTemplate.Grade.S84) {
            return additionalSkillChance[12];
        }
        return 0;
    }

    @Deprecated
    private static long getCurrentPrice(ItemInstance item) {
        long price = 0;

        if (item.getTemplate().getItemGrade() == ItemTemplate.Grade.NONE) {
            price =randomStartPrices[0];
        }
        if (item.getTemplate().getItemGrade() == ItemTemplate.Grade.D) {
            price = randomStartPrices[1];
        }
        if (item.getTemplate().getItemGrade() == ItemTemplate.Grade.C) {
            price = randomStartPrices[2];
        }
        if (item.getTemplate().getItemGrade() == ItemTemplate.Grade.B) {
            price = randomStartPrices[3];
        }
        if (item.getTemplate().getItemGrade() == ItemTemplate.Grade.A) {
            price = randomStartPrices[4];
        }
        if (item.getTemplate().getItemGrade() == ItemTemplate.Grade.S) {
            price = randomStartPrices[5];
        }
        if (item.getTemplate().getItemGrade() == ItemTemplate.Grade.S80) {
            price = randomStartPrices[6];
        }
        if (item.getTemplate().getItemGrade() == ItemTemplate.Grade.S84) {
            price = randomStartPrices[7];
        }

        if (item.getRandomCount() != 0) {
//            //double dPrice = (price *randomPriceMul) * item.getRandomCount();
//            price = item.getRandomPrice() * 2;
            double dPrice = (price *randomPriceMul) * item.getRandomCount();
            price = Math.round(dPrice);

        }

        return price;
    }

    public static Integer[] getDestroyItemCurrencyBackAmount() {
        return destroyItemCurrencyBackAmount;
    }

    public static Integer[] getLegitBodyParts() {
        return legitBodyParts;
    }

    public static Integer getDestroyItemMaxSkillDivider() {
        return destroyItemMaxSkillDivider;
    }

    public static Integer getDestroyItemDividedMaxLvl(ItemInstance item) {
        Integer maxLvl = 0;
        for (Skill sk : item.getItemInstanceSkills()) {
            if (sk.getLevel() > maxLvl) {
                maxLvl = sk.getLevel();
            }
        }
        return maxLvl / destroyItemMaxSkillDivider;
    }

    public static Integer getGetFixedItemChanceAdd() {
        return getFixedItemChanceAdd;
    }

    public static void setGetFixedItemChanceAdd(Integer getFixedItemChanceAdd) {
        StatsRndManager.getFixedItemChanceAdd = getFixedItemChanceAdd;
    }

    public static Integer getGetFixedItemChanceAddLimit() {
        return getFixedItemChanceAddLimit;
    }

    public static void setGetFixedItemChanceAddLimit(Integer getFixedItemChanceAddLimit) {
        StatsRndManager.getFixedItemChanceAddLimit = getFixedItemChanceAddLimit;
    }

    public static Integer getGetFixedItemGroupChanceAdd() {
        return getFixedItemGroupChanceAdd;
    }

    public static void setGetFixedItemGroupChanceAdd(Integer getFixedItemGroupChanceAdd) {
        StatsRndManager.getFixedItemGroupChanceAdd = getFixedItemGroupChanceAdd;
    }

    public static Integer getGetFixedItemGroupChanceAddLimit() {
        return getFixedItemGroupChanceAddLimit;
    }

    public static void setGetFixedItemGroupChanceAddLimit(Integer getFixedItemGroupChanceAddLimit) {
        StatsRndManager.getFixedItemGroupChanceAddLimit = getFixedItemGroupChanceAddLimit;
    }

    public static Integer[] getItemsCBLvls() {
        return itemsCBLvls;
    }

    public static void setItemsCBLvls(Integer[] itemsCBLvls) {
        StatsRndManager.itemsCBLvls = itemsCBLvls;
    }

    public static Integer getMaxSkillsInItem() {
        return maxSkillsInItem;
    }

    public static void setMaxSkillsInItem(Integer maxSkillsInItem) {
        StatsRndManager.maxSkillsInItem = maxSkillsInItem;
    }

    public static String formatSkillsShort(List<Skill> itemInstanceSkills) {
        String result = "";
        for (Skill s : itemInstanceSkills) {
            String fullName = s.getName();
            String shortName = getShortName(fullName);
            result += String.format("[%s:%d]", shortName, s.getLevel());
        }
        return result;
    }

    private static String getShortName(String fullName) {
        String result = "";
        result = fullName.replace("Item ", "")
                            .replace(" Mastery Body", "")
                            .replace(" Mastery Head", "")
                            .replace(" Mastery Gloves", "")
                            .replace(" Mastery Weapon", "")
                            .replace(" Mastery Shoes", "");
        return result;
    }
}
