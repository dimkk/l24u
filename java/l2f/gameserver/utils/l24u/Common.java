package l2f.gameserver.utils.l24u;

import l2f.gameserver.model.Player;
import l2f.gameserver.model.Skill;
import l2f.gameserver.tables.SkillTable;
import l2f.commons.l24u.Communication.CommunicationObject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Dima on 01.09.2016.
 */
public class Common {
    protected static final Logger _log = LoggerFactory.getLogger("l24uCommon");



    public static void broadCastToPlayers(List<Player> players, String message){
        for (Player p : players){
            p.sendStringMessage(message);
        }
    }

    public static boolean contIgnoreCase(String source, String subj){
        return StringUtils.containsIgnoreCase(source, subj);
    }

    public static int getRndBetween(int low, int high){
        Random r = new Random();
        if (low == 1 && high == 2) return r.nextBoolean() ? low : high;
        int Result = r.nextInt(high-low) + low;
        return Result;
    }

    public static double calculateAverage(List <Integer> marks) {
        Integer sum = 0;
        if(!marks.isEmpty()) {
            for (Integer mark : marks) {
                sum += mark;
            }
            return sum.doubleValue() / marks.size();
        }
        return sum;
    }
    public static List<String> createSkillGrpStrings(Integer skillId){
            List<String> result = new ArrayList<String>();
            Integer maxLvl = SkillTable.getInstance().getMaxLevel(skillId);
            for (int i = 1; i <= maxLvl; i++){
                Skill s = SkillTable.getInstance().getInfo(skillId, i);
                String str = s.getId() + "\t"+s.getLevel()+"\t16\t2\t0\t400\t0\t0.00000000\t0\t\t\t"+ s.getIcon() +"\t\t0\t0\t0\ta,none\\0\t0\t-1\t-1\t0\ta,none\\0";
                result.add(str);
        }
        return result;
    }

    public static List<String> createSkillNameStrings(Integer skillId){
        List<String> result = new ArrayList<String>();
        Integer maxLvl = SkillTable.getInstance().getMaxLevel(skillId);
        for (int i = 1; i <= maxLvl; i++){
            Skill s = SkillTable.getInstance().getInfo(skillId, i);
            String pattern = s.getId() + "\t"+s.getLevel()+"\tu,"+s.getName()+"\\0\tu, l24u \\0\ta,none\\0\ta,none\\0";
            result.add(pattern);
        }
        return result;
    }

    public static void writeToFile(String fileName, String contents){
        try {
            FileUtils.write(new File(fileName), contents, "UTF-8");
        } catch (IOException e) {
            _log.error("errrr", e);
        }
    }

    public static void generateStringForSkills(){
        List<String> grp = new ArrayList<String>();
        List<String> names = new ArrayList<String>();
        Integer startSkillId = 90100;
        Integer currentSkillId = startSkillId;
        Skill s = SkillTable.getInstance().getInfo(currentSkillId, 1);
        while (s != null){
            grp.addAll(createSkillGrpStrings(currentSkillId));
            names.addAll(createSkillNameStrings(currentSkillId));
            try{
                currentSkillId++;
                s = SkillTable.getInstance().getInfo(currentSkillId, 1);
            } catch( Exception ex){
                _log.error("errrr", ex);
            }
        }
        try {
            FileUtils.writeLines(new File("grp.txt"), grp);
            FileUtils.writeLines(new File("names.txt"), names);
        } catch (IOException e) {
            _log.error("errrr", e);
        }
    }

    /**
     * Парсит строку вида diablo_section_method_arg1 arg2 arg3 ... в объект CommunicationObject
     * @param bypass diablo_section_method_arg1 arg2 arg3
     * @return
     */
    public static CommunicationObject getCommunicationObjectFromBypass(String bypass) {
        String command = bypass.trim();
        String[] array = command.split("_").clone();
        String section = array[1];
        String method = array[2];
        array = ArrayUtils.removeElement(array, array[0]); //diablo
        array = ArrayUtils.removeElement(array, section);//section
        array = ArrayUtils.removeElement(array, method);//method
        if (array.length > 0){
            String lastElement = array[array.length - 1];
            String[] spaceArgs = lastElement.split(" ");
            if (spaceArgs.length > 1)
                array = spaceArgs;
        }
        CommunicationObject obj = new CommunicationObject(section, method, array);
        return obj;
    }
}
