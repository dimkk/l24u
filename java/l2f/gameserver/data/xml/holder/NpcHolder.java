package l2f.gameserver.data.xml.holder;

import gnu.trove.iterator.TIntObjectIterator;
import gnu.trove.map.hash.TIntObjectHashMap;
import l2f.commons.data.xml.AbstractHolder;
import l2f.commons.lang.ArrayUtils;
import l2f.gameserver.model.GameObjectsStorage;
import l2f.gameserver.model.Player;
import l2f.gameserver.model.instances.MinionInstance;
import l2f.gameserver.model.instances.MonsterInstance;
import l2f.gameserver.model.instances.PetInstance;
import l2f.gameserver.model.reward.RewardGroup;
import l2f.gameserver.model.reward.RewardList;
import l2f.gameserver.model.reward.RewardType;
import l2f.gameserver.templates.npc.NpcTemplate;
import l2f.gameserver.utils.l24u.Common;
import l2f.gameserver.utils.l24u.diablo.DiabloNpcGenerator;
import l2f.gameserver.utils.l24u.diablo.RiftStage;

import javax.xml.crypto.dsig.keyinfo.KeyValue;
import java.lang.reflect.Array;
import java.util.*;

public final class NpcHolder extends AbstractHolder
{
	private static final NpcHolder _instance = new NpcHolder();

	private TIntObjectHashMap<NpcTemplate> _npcs = new TIntObjectHashMap<NpcTemplate>(20000);
	private TIntObjectHashMap<List<NpcTemplate>> _npcsByLevel;
	private NpcTemplate[] _allTemplates;
	private Map<String, NpcTemplate> _npcsNames;

	public static NpcHolder getInstance()
	{
		return _instance;
	}

	NpcHolder()
	{

	}

	public void addTemplate(NpcTemplate template)
	{
		_npcs.put(template.npcId, template);
	}

	public NpcTemplate getTemplate(int id)
	{
		NpcTemplate npc = ArrayUtils.valid(_allTemplates, id);
		if (npc == null)
		{
			//npc = getNpcTemplateFromRiftStages(id);
			if (npc == null) {
				warn("Not defined npc id : " + id + ", or out of range!", new Exception());
				return null;
			}
		}
		return _allTemplates[id];
	}

	private NpcTemplate getNpcTemplateFromRiftStages(int id) {
		NpcTemplate npc = null;
		for (Player player : GameObjectsStorage.getAllPlayersForIterate())
        {
            for (Map.Entry<Integer,RiftStage> rs: player.get_riftGenerator().get_riftStages().entrySet()){
                if (rs.getValue().get_mobSpawner().getNpcTemplate(id) != null){
                    npc = rs.getValue().get_mobSpawner().getNpcTemplate(id);
                }
            }
        }
		return npc;
	}

	public NpcTemplate getRandomNpc(){
		Object[] target = _npcs.valueCollection().toArray();
		int random = Common.getRndBetween(0, target.length);
		NpcTemplate template = (NpcTemplate)target[random];
		if (template.type == null){
			getRandomNpc();
		}
		return template;
	}

	public NpcTemplate getRandomNpcByLevel(int lvl){
		int random = Common.getRndBetween(0, _npcsByLevel.get(lvl).size());
		NpcTemplate template = _npcsByLevel.get(lvl).get(random);
		if (template.type == null){
			getRandomNpcByLevel(lvl);
		}
		return template;
	}

	public NpcTemplate getTemplateByName(String name)
	{
		return _npcsNames.get(name.toLowerCase());
	}

	public List<NpcTemplate> getAllOfLevel(int lvl)
	{
		return _npcsByLevel.get(lvl);
	}

	public NpcTemplate[] getAll()
	{
		return _npcs.values(new NpcTemplate[_npcs.size()]);
	}

	private void buildFastLookupTable()
	{
		_npcsByLevel = new TIntObjectHashMap<List<NpcTemplate>>();
		_npcsNames = new HashMap<String, NpcTemplate>();

		int highestId = 0;
		for (int id : _npcs.keys())
			if (id > highestId)
				highestId = id;

		_allTemplates = new NpcTemplate[highestId + 1];
		for (TIntObjectIterator<NpcTemplate> iterator = _npcs.iterator(); iterator.hasNext();)
		{
			iterator.advance();
			int npcId = iterator.key();
			NpcTemplate npc = iterator.value();

			_allTemplates[npcId] = npc;

			List<NpcTemplate> byLevel;
			if ((byLevel = _npcsByLevel.get(npc.level)) == null)
				_npcsByLevel.put(npc.level, byLevel = new ArrayList<NpcTemplate>());
			byLevel.add(npc);

			_npcsNames.put(npc.name.toLowerCase(), npc);
		}
	}

	@Override
	protected void process()
	{
		buildFastLookupTable();
	}

	@Override
	public int size()
	{
		return _npcs.size();
	}

	@Override
	public void clear()
	{
		_npcsNames.clear();
		_npcs.clear();
	}

	public List<NpcTemplate> getTemplatesByRace(List<NpcTemplate> list, int race) {
		List<NpcTemplate> result = new ArrayList<NpcTemplate>();
		HashMap<Integer, List<NpcTemplate>> byRaceId = new HashMap<Integer, List<NpcTemplate>>();
		for (NpcTemplate template: list){
			if (!byRaceId.containsKey(template.getRace())) {
				List<NpcTemplate> templist = new ArrayList<NpcTemplate>();
				templist.add(template);
				byRaceId.put(template.getRace(), templist);
			} else {
				byRaceId.get(template.getRace()).add(template);
			}
		}
		result = byRaceId.get(race);
		return  result;
	}

	public NpcTemplate getRandomNpcByLevelsRange(int riftMobLvl, int diabloMobsLvlRangeMin, int diabloMobsLvlRangeMax) {
		if (riftMobLvl == 1) riftMobLvl = diabloMobsLvlRangeMin + riftMobLvl;
		int fromLvl = riftMobLvl - diabloMobsLvlRangeMin;
		int toLvl = riftMobLvl + diabloMobsLvlRangeMax;
		List<NpcTemplate> result = new ArrayList<NpcTemplate>();

		while (fromLvl != toLvl){
			if (_npcsByLevel.get(fromLvl) != null)
				result.addAll(_npcsByLevel.get(fromLvl));
			fromLvl++;
		}

		result = DiabloNpcGenerator.getInstance().filterNpc(result);
		int random = Common.getRndBetween(0, result.size() - 1);
		NpcTemplate template = result.get(random);

		return template;
	}
}
