package l2f.gameserver.dao;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import l2f.commons.dbutils.DbUtils;
import l2f.gameserver.Config;
import l2f.gameserver.database.DatabaseFactory;
import l2f.gameserver.database.mysql;
import l2f.gameserver.model.Player;
import l2f.gameserver.model.items.ItemInstance;
import l2f.gameserver.utils.Location;
import l2f.gameserver.utils.Strings;
import l2f.gameserver.utils.l24u.diablo.models.DiabloItemsObj;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class diabloDAO
{
	private static final Logger _log = LoggerFactory.getLogger(diabloDAO.class);

	private static diabloDAO _instance = new diabloDAO();

	public static diabloDAO getInstance()
	{
		return _instance;
	}

	public static List<DiabloItemsObj> getDiabloItemObjectForItem(Integer objId){
		String itemsRaw = getJsonForItem(objId);
		if (!com.google.common.base.Strings.isNullOrEmpty(itemsRaw)) {
			Gson gson = new GsonBuilder().create();
			Type listType = new TypeToken<ArrayList<DiabloItemsObj>>() {
			}.getType();
			List<DiabloItemsObj> items = gson.fromJson(itemsRaw, listType);
			return items;
		} else {
			return new ArrayList<DiabloItemsObj>();
		}
	}

	public static void setJsonForList(int objId, List<DiabloItemsObj> items){
		// В базе много пустых записей
		if (items.size() > 0) {
			Gson gson = new GsonBuilder().create();
			Type listType = new TypeToken<ArrayList<DiabloItemsObj>>() {
			}.getType();
			String result = gson.toJson(items, listType);
			// В базе много пустых записей
			if (!result.equals("[]"))
				setJsonForItem(objId, result);
		}
	}

	public static void delete(Integer objId)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("DELETE FROM diablo_items WHERE objectId = ?");
			statement.setInt(1, objId);
			statement.execute();
		}
		catch (Exception ex){
			_log.error("err", ex);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	private static void setJsonForItem(int objId, String value)
	{
		mysql.set("REPLACE INTO diablo_items (objectId, skillsLevelsJson) VALUES (?,?)", objId, value);
	}

	private static String getJsonForItem(int objId)
	{
		String value = null;

		try (Connection con = DatabaseFactory.getInstance().getConnection();
			 PreparedStatement offline = con.prepareStatement("SELECT skillsLevelsJson FROM diablo_items WHERE objectId = ?"))
		{
			offline.setInt(1, objId);

			try (ResultSet rs = offline.executeQuery())
			{
				if(rs.next())
				{
					value = Strings.stripSlashes(rs.getString("skillsLevelsJson"));
				}
			}
		}
		catch(Exception e)
		{
			_log.error("Error while getting Variable from Player", e);
		}
		return value;
	}


	public static void setRandomCountForItem(int objId, int randomCount) {
		List<DiabloItemsObj> items = getDiabloItemObjectForItem(objId);
		for (DiabloItemsObj item:items){
			item.setRandomCount(randomCount);
		};
		setJsonForList(objId, items);
	}

	public static void setRandomPriceForItem(int objectId, long randomPrice) {
		List<DiabloItemsObj> items = getDiabloItemObjectForItem(objectId);
		for (DiabloItemsObj item:items){
			item.setRandomPrice(randomPrice);
		};
		setJsonForList(objectId, items);
	}
}