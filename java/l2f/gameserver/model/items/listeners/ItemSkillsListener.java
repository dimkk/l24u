package l2f.gameserver.model.items.listeners;

import l2f.gameserver.Config;
import l2f.gameserver.listener.inventory.OnEquipListener;
import l2f.gameserver.model.Playable;
import l2f.gameserver.model.Player;
import l2f.gameserver.model.Skill;
import l2f.gameserver.model.Summon;
import l2f.gameserver.model.items.ItemInstance;
import l2f.gameserver.network.serverpackets.SkillCoolTime;
import l2f.gameserver.network.serverpackets.SkillList;
import l2f.gameserver.stats.Formulas;
import l2f.gameserver.tables.SkillTable;
import l2f.gameserver.templates.item.ItemTemplate;
import l2f.gameserver.utils.l24u.diablo.StatsRndManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class ItemSkillsListener implements OnEquipListener
{
	private static final ItemSkillsListener _instance = new ItemSkillsListener();

	public static ItemSkillsListener getInstance()
	{
		return _instance;
	}

	@Override
	public void onUnequip(int slot, ItemInstance item, Playable actor)
	{
		Player player = (Player)actor;

		Skill[] itemSkills = null;
		Skill enchant4Skill = null;

		ItemTemplate it = item.getTemplate();
		boolean setPetSkills = false;
		if (player.getPet() != null) {
			setPetSkills = true;
		}
		List<Skill> temp = new ArrayList<Skill>();
		temp.addAll(Arrays.asList(it.getAttachedSkills()));
		temp.addAll(item.getItemInstanceSkills());

		itemSkills = temp.toArray(new Skill[temp.size()]);

		enchant4Skill = it.getEnchant4Skill();

		player.removeTriggers(it);

		if (itemSkills != null && itemSkills.length > 0)
			for (Skill itemSkill : itemSkills)
				if (itemSkill.getId() >= 26046 && itemSkill.getId() <= 26048)
				{
					int level = player.getSkillLevel(itemSkill.getId());
					int newlevel = level - 1;
					if (newlevel > 0)
						player.addSkill(SkillTable.getInstance().getInfo(itemSkill.getId(), newlevel), false);
					else {
						player.removeSkillById(itemSkill.getId());
						removePetSkill(setPetSkills, itemSkill.getId(), player);
					}

				}
				else
				{
					player.removeSkill(itemSkill, false);
					removePetSkill(setPetSkills, itemSkill.getId(), player);
				}

		if (enchant4Skill != null)
			player.removeSkill(enchant4Skill, false);

		if (itemSkills.length > 0 || enchant4Skill != null)
		{
			player.sendPacket(new SkillList(player));
			player.updateStats();
		}
	}

	void addPetSkill(boolean setPetSkills, Skill itemSkill, Player player, Integer newlevel){
		if (setPetSkills && StatsRndManager.isDiabloItemSkill(itemSkill.getId())) {
			Summon pet = player.getPet();
			pet.addSkill(SkillTable.getInstance().getInfo(itemSkill.getId(), newlevel));
			pet.sendStatusUpdate();
		}
	}

	void removePetSkill(boolean setPetSkills, Integer skillId, Player player){
		if (setPetSkills) {
			Summon pet = player.getPet();
			pet.removeSkillById(skillId);
			pet.sendStatusUpdate();
		}
	}

	@Override
	public void onEquip(int slot, ItemInstance item, Playable actor)
	{
		Player player = (Player)actor;

		Skill[] itemSkills = null;
		Skill enchant4Skill = null;

		ItemTemplate it = item.getTemplate();

		List<Skill> temp = new ArrayList<Skill>();
		temp.addAll(Arrays.asList(it.getAttachedSkills()));
		temp.addAll(item.getItemInstanceSkills());

		itemSkills = temp.toArray(new Skill[temp.size()]);

		if (item.getEnchantLevel() >= 4)
			enchant4Skill = it.getEnchant4Skill();

		// Для оружия при несоотвествии грейда скилы не выдаем
		if (it.getType2() == ItemTemplate.TYPE2_WEAPON && player.getWeaponsExpertisePenalty() > 0)
			return;

		player.addTriggers(it);
		boolean setPetSkills = false;
		if (player.getPet() != null) {
			setPetSkills = true;
		}

		boolean needSendInfo = false;
		if (itemSkills.length > 0)
			for (Skill itemSkill : itemSkills)
				if (itemSkill.getId() >= 26046 && itemSkill.getId() <= 26048)
				{
					int level = player.getSkillLevel(itemSkill.getId());
					int newlevel = level;
					if (level > 0)
					{
						if (SkillTable.getInstance().getInfo(itemSkill.getId(), level + 1) != null)
							newlevel = level + 1;
					}
					else
						newlevel = 1;
					if (newlevel != level)
					{
						player.addSkill(SkillTable.getInstance().getInfo(itemSkill.getId(), newlevel), false);
						addPetSkill(setPetSkills, itemSkill, player, newlevel / 2);
						if (Config.ALT_DEBUG_ENABLED) player.sendMessage("skill added - " + itemSkill.getId());
					}
				}
				else if (player.getSkillLevel(itemSkill.getId()) < itemSkill.getLevel())
				{
					player.addSkill(itemSkill, false);
					addPetSkill(setPetSkills, itemSkill, player, itemSkill.getLevel() / 2);
					if (Config.ALT_DEBUG_ENABLED) player.sendMessage("skill added - " + itemSkill.getId());

					if (itemSkill.isActive())
					{
						long reuseDelay = Formulas.calcSkillReuseDelay(player, itemSkill);
						reuseDelay = Math.min(reuseDelay, 30000);

						if (reuseDelay > 0 && !player.isSkillDisabled(itemSkill))
						{
							player.disableSkill(itemSkill, reuseDelay);
							needSendInfo = true;
						}
					}
				}

		if (enchant4Skill != null)
			player.addSkill(enchant4Skill, false);

		if (itemSkills.length > 0 || enchant4Skill != null)
		{
			player.sendPacket(new SkillList(player));
			player.updateStats();
			if (needSendInfo)
				player.sendPacket(new SkillCoolTime(player));
		}
	}
}