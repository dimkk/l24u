package l2f.gameserver.model.entity.auction;

public enum SkillTypes implements AuctionItemTypes
{
	PAtck,
	MAtck,
	Ancient_sword,
	Blunt,
	Big_blunt,
	Dagger,
	Dual_dagger,
	Bow,
	Crossbow,
	Pole,
	Fists,
	Rapier,
	Other
}
