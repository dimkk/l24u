package l2f.gameserver.model.instances.diablo.rift;

import l2f.gameserver.data.htm.HtmCache;
import l2f.gameserver.model.Player;
import l2f.gameserver.model.instances.NpcInstance;
import l2f.gameserver.network.serverpackets.NpcHtmlMessage;
import l2f.gameserver.templates.npc.NpcTemplate;
import l2f.gameserver.utils.l24u.diablo.RiftGenerator;
import l2f.gameserver.utils.l24u.diablo.TerritorySearcher;

/**
 * Created by dimkk on 11/7/2016.
 */
public class RiftExitInstance extends NpcInstance {
    public RiftExitInstance(int objectId, NpcTemplate template)
    {
        super(objectId, template);
    }
    private static final String HTML_TEST = "diablo/rift/exit.htm";
    @Override
    public void onBypassFeedback(Player player, String command) {
        if (!canBypassCheck(player, this))
        {
            return;
        }

        if (command.startsWith("Rift"))
            try
            {
                player.get_riftGenerator().startRift();
            }
            catch (NumberFormatException nfe)
            {
            }
        if (command.startsWith("StartPoint"))
            try
            {
                player.teleToLocation(player.get_riftGenerator().get_riftStages().get(player.get_currentRiftStage()).getRiftLoc().getP1(), player.getReflectionId());
            }
            catch (NumberFormatException nfe)
            {
            }

        player.sendPacket(new NpcHtmlMessage(player, this, HtmCache.getInstance().getNotNull(HTML_TEST, player), 0));
    }
    @Override
    public void showChatWindow(Player player, int val, Object... arg)
    {
        player.sendPacket(new NpcHtmlMessage(player, this, HtmCache.getInstance().getNotNull(HTML_TEST, player), val));
    }
}
