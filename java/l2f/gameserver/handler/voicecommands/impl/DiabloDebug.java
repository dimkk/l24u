package l2f.gameserver.handler.voicecommands.impl;

import l2f.gameserver.utils.l24u.Common;
import l2f.gameserver.handler.voicecommands.IVoicedCommandHandler;
import l2f.gameserver.model.Player;
import l2f.gameserver.network.serverpackets.ExShowTrace;
import l2f.gameserver.scripts.Functions;
import l2f.commons.l24u.Communication.CommunicationObject;
import l2f.gameserver.utils.l24u.diablo.L24uDistribution;
import l2f.gameserver.utils.l24u.diablo.RiftStage;

public class DiabloDebug extends Functions implements IVoicedCommandHandler
{
	private static final String[] _commandList = {"zoneedit","rpctest", "dia", "diap", "diaspawn", "diaspawndelete", "drift", "dtrace", "diainfo", "diaspawnadd", "cstr"};
    private static int count;

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}

	@Override
	public boolean useVoicedCommand(String command, Player player, String args)
	{
        try {
            if (command.equals("zoneedit")){
                CommunicationObject obj = new CommunicationObject("riftEditor", "showZones", new String[]{""});
                obj.addHash("playerId", Integer.toString(player.getObjectId()));
                obj.addHash("coords", player.getLoc().toString());
                new L24uDistribution(player).communicate(obj);
            }
            if (command.equals("diainfo")){
                this.sendInfo(player);
            }
            if (command.equals("dtrace")){
                if (player.getTarget() == null) {
                    player.sendMessage("Надо взять когото в цель");
                    return false;
                }
                ExShowTrace trace = new ExShowTrace();
                trace.addLine(player.getLoc(), player.getTarget().getLoc(), 30, 600000);
                player.sendPacket(trace);
            }
            if (command.equals("cstr")){
                Common.generateStringForSkills();
            }
            if (command.equals("drift")){
                if (args.length() > 0) {
                    player.get_riftGenerator().useOldRiftGenerationMechanism = false;
                } else {
                    player.get_riftGenerator().useOldRiftGenerationMechanism = true;
                }
                    player.get_riftGenerator().startRift();
            }
            if (command.equals("dia")){
                if (count == 0){
                    player.teleToLocation(player.get_riftGenerator().get_riftStages().get(player.get_currentRiftStage()).getRiftLoc().getP1(), player.getReflectionId());
                    count++;
                    return true;
                }
                if (count == 1){
                    player.teleToLocation(player.get_riftGenerator().get_riftStages().get(player.get_currentRiftStage()).getRiftLoc().getP2(), player.getReflectionId());
                    count--;
                    return true;
                }
            }
            if (command.equals("diap")){
                player.teleToLocation(player.get_riftGenerator().get_riftStages().get(player.get_currentRiftStage()).get_mobSpawner().getNextLoc(), player.getReflectionId());
            }
            if (command.equals("diaspawn")){
                RiftStage currentStage =  player.get_riftGenerator().get_riftStages().get(player.get_currentRiftStage());
                currentStage.ResetMobsSpawner();
                if (!args.isEmpty()) {
                    String[] arg = args.split(" ");
                    currentStage.get_mobSpawner().setMaxMobsCount(Integer.parseInt(arg[0]));
                    currentStage.get_mobSpawner().setMaxMobsCount(Integer.parseInt(arg[1]));
                }
                currentStage.get_mobSpawner().testSpawnInLocs(player.getReflection(), player);
            }
            if (command.equals("diaspawnadd")){
                RiftStage currentStage =  player.get_riftGenerator().get_riftStages().get(player.get_currentRiftStage());
                if (!args.isEmpty()) {
                    String[] arg = args.split(" ");
                    currentStage.get_mobSpawner().setMaxMobsCount(Integer.parseInt(arg[0]));
                    currentStage.get_mobSpawner().setMaxMobsCount(Integer.parseInt(arg[1]));
                }
                currentStage.get_mobSpawner().testSpawnInLocs(player.getReflection(), player);
            }
            if (command.equals("diaspawndelete")){
                RiftStage currentStage =  player.get_riftGenerator().get_riftStages().get(player.get_currentRiftStage());
                currentStage.ResetMobsSpawner();
            }
        } catch (Exception ex){
            this.sendInfo(player);
        }

        return false;

	}
    private void sendInfo(Player player){
        player.sendMessage("команды: .drift в рифт - .drift new - будет генерить комнаты из нового механизма, просто .drift - опять будет генерить по старому");
        player.sendMessage(".dia - прыгаем по начальной и конечно точкам");
        player.sendMessage(".diaspawn x y - наспаунить мобов - x - общее кол-во, y - кол-во видов (делится поровну)");
        player.sendMessage(".diaspawndelete - удалит отспауненых мобов");
        player.sendMessage(".diaspawnadd x - добавит мобов, где х - количество");
    }

}
