package l2f.gameserver.handler.voicecommands.impl;

import l2f.gameserver.Config;
import l2f.gameserver.handler.voicecommands.IVoicedCommandHandler;
import l2f.gameserver.model.Player;
import l2f.gameserver.network.serverpackets.components.CustomMessage;

public class Torment implements IVoicedCommandHandler
{
	private static final String[] _commandList = {"t", "tlvl"};

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}

	@Override
	public boolean useVoicedCommand(String command, Player player, String args)
	{
		if (!Config.TORMENT_ENABLE)
			return false;

        if (command.equals("t")){
            String currentTorment = player.getSessionVar("torment");

            if (args.length() == 0 && currentTorment != null && currentTorment.length() > 0) {
                player.sendStringMessage("Текущее истязание - " + currentTorment);
            }

            if (args.length() == 0 && currentTorment == null){
                player.setSessionVar("torment", "1");
                player.sendStringMessage("Истязание - 1, чтобы изменить на 10 - .t 10, может быть от 1 до " + Config.TORMENT_LVLS);
            }

            if (args.length() > 0){
                String newTorment = args.toString();
                Integer newTCheck;
                try{
                    newTCheck = Integer.parseInt(newTorment);
                } catch (Exception ex) {
                    player.sendStringMessage("Истязание может быть только 1 - " + Config.TORMENT_LVLS);
                    return false;
                }
                if (newTCheck > Config.TORMENT_LVLS || newTCheck < 0){
                    player.sendStringMessage("Истязание может быть только 1 - " + Config.TORMENT_LVLS);
                    return false;
                }
                player.setSessionVar("torment", newTCheck.toString());
                player.sendStringMessage("Истязание - " + newTCheck);
            }
        }

        if (command.equals("tlvl")){
            int currentTorment = Config.TORMENT_STAGE;

            if (args.length() == 0 && currentTorment > 0) {
                player.sendStringMessage("Текущее вариант истязания - " + currentTorment);
            }

            if (args.length() > 0){
                String newTorment = args.toString();
                Integer newTStage;
                try{
                    newTStage = Integer.parseInt(newTorment);
                } catch (Exception ex) {
                    player.sendStringMessage("Вариант истязания - 1 2 или 3");
                    return false;
                }
                if (newTStage > 3|| newTStage < 0){
                    player.sendStringMessage("Вариант истязания - 1 2 или 3");
                    return false;
                }
                Config.TORMENT_STAGE = newTStage;
                player.sendStringMessage("Истязание вариант - " + newTStage);
            }
        }
		return true;
	}
}
