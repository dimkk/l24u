package l2f.gameserver.handler.bbs.impl;

import java.util.*;

import l2f.commons.dao.JdbcEntityState;
import l2f.gameserver.Config;
import l2f.gameserver.data.htm.HtmCache;
import l2f.gameserver.handler.bbs.CommunityBoardManager;
import l2f.gameserver.handler.bbs.ICommunityBoardHandler;
import l2f.gameserver.model.Player;
import l2f.gameserver.model.Skill;
import l2f.gameserver.model.items.ItemInstance;
import l2f.gameserver.network.ClientHelpers;
import l2f.gameserver.network.serverpackets.EnchantResult;
import l2f.gameserver.network.serverpackets.ShowBoard;
import l2f.gameserver.scripts.ScriptFile;
import l2f.gameserver.tables.SkillTable;
import l2f.gameserver.utils.Log;

import l2f.gameserver.utils.PositionUtils;
import l2f.gameserver.utils.l24u.Common;
import l2f.commons.l24u.Communication.CommunicationObject;
import l2f.gameserver.utils.l24u.diablo.L24uDistribution;
import l2f.gameserver.utils.l24u.diablo.StatsRndManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Community Board page containing Item randomizer and tools
 */
public class CommunityDiabloItems implements ScriptFile, ICommunityBoardHandler
{
	private static final Logger _log = LoggerFactory.getLogger(CommunityDiabloItems.class);

	@Override
	public void onLoad()
	{
		if(Config.COMMUNITYBOARD_ENABLED)
		{
			_log.info("CommunityBoard: DiabloItemsLoaded.");
			CommunityBoardManager.getInstance().registerHandler(this);
		}
	}

	@Override
	public void onReload()
	{
		if(Config.COMMUNITYBOARD_ENABLED)
			CommunityBoardManager.getInstance().removeHandler(this);
	}

	@Override
	public void onShutdown()
	{}

	@Override
	public String[] getBypassCommands()
	{
		return new String[] {
				"_bbslink",
				"diablo_statsRnd",
//				"_dropItemsByName",
//				"_dropMonstersByItem",
//				"_dropMonstersByName",
//				"_dropMonsterDetailsByItem",
//				"_dropMonsterDetailsByName"
		};
	}

	@Override
	public void onBypassCommand(Player player, String bypass) {
		if (!Config.DIABLO_RND_STATS) {
			String html = HtmCache.getInstance().getNotNull(Config.BBS_HOME_DIR + "bbs_dropCalcOff.htm", player);
			ShowBoard.separateAndSend(html, player);
			return;
		}
		CommunicationObject obj;
		if(bypass.contains("bbslink"))
		{
			obj = new CommunicationObject("statsRnd", "itemsList");
		} else {
			obj = Common.getCommunicationObjectFromBypass(bypass);
		}

		obj = prepareCommunication(obj, player);
		new L24uDistribution(player).communicate(obj);
		return;
	}

	public static CommunicationObject prepareCommunication(CommunicationObject obj, Player player) {
		if (obj.getMethod().contains("itemsList")) {
			obj = prepareList(obj, player);
			obj.addHash("playerId", Integer.toString(player.getObjectId()));
			obj.addHash("items", StatsRndManager.getItemsSortedByIdToSend(player));
		} else {
			Integer currencyId = 0;
			Long price = 0l;
			if (obj.getMethod().contains("Admin")) {
				if (player.isGM() && player.getTarget() != null && player.getTarget().isPlayer()) {
					obj = prepareAdmin(obj, player, (Player)player.getTarget());
				} else {
					obj = prepareAdmin(obj, player, player);
				}

			} else {
				obj = prepare(obj, player);
			}
			obj.addHash("playerId", Integer.toString(player.getObjectId()));
			obj.addHash("lvl", Integer.toString(player.getLevel()));
			obj.addHash("currentAdena", Long.toString(player.getAdena()));
			obj.addHash("isAdmin", Boolean.toString(player.isGM()));
			if (player.isGM() && player.getTarget() != null && player.getTarget().isPlayer()) {
				obj.addHash("items", StatsRndManager.getItemsToSend((Player)player.getTarget()));
			} else {
				obj.addHash("items", StatsRndManager.getItemsToSend(player));
			}
		}
		return obj;
	}

	private static CommunicationObject prepareList(CommunicationObject obj, Player player) {
		obj.removeByKey("error");
		if (obj.getMethod().equals("itemsListEqUneq") && obj.getArgs().length > 0){
			Integer itemId = Integer.parseInt(obj.getArgs()[0]);
			ClientHelpers.UseItem(player, itemId, false);
//			if (!success) {
//				obj.addHash("error", "Не получилось одеть / снять предмет");
//			}
		}
		if (obj.getMethod().equals("itemsListDrop") && obj.getArgs().length > 0){
			Integer itemId = Integer.parseInt(obj.getArgs()[0]);
			ClientHelpers.DropItem(player, 1L, PositionUtils.getRandomLocNearPlayer(player), itemId);
		}
		if (obj.getMethod().equals("itemsListSell") && obj.getArgs().length > 0){
			if (!player.isInZonePeace()) {
				obj.addHash("error", "Нельзя продавать вне безопасной зоны!");
				return obj;
			}
			Integer itemId = Integer.parseInt(obj.getArgs()[0]);
			ItemInstance item = player.getInventory().getItemByItemObjId(itemId);

			if (item.isEquipped()) {
				player.getInventory().unEquipItem(item);
			}
			ClientHelpers.SellItem(player, 1, new int[]{itemId}, new long[]{1L});
		}
		if (obj.getMethod().equals("itemsListWh") && obj.getArgs().length > 0){
			if (!player.isInZonePeace()) {
				obj.addHash("error", "Нельзя положить на склад  вне безопасной зоны!");
				return obj;
			}
			Integer itemId = Integer.parseInt(obj.getArgs()[0]);
			ItemInstance item = player.getInventory().getItemByItemObjId(itemId);
			if (item.isEquipped()) {
				player.getInventory().unEquipItem(item);
			}
			ClientHelpers.DepositItems(player, 1, new int[]{itemId}, new long[]{1L}, 30L);
		}

		return obj;
	}

	private static CommunicationObject prepareAdmin(CommunicationObject obj, Player user, Player player) {
		if (!user.isGM()) {
			obj.addHash("error", "Player is not GM");
			Log.LogSecurity("ALARM:SECURITY:DIABLOITEMS", player, "Attempt to use admin command by non gm player!");
		}
		if (obj.getMethod().equals("showAdminSetSkillAndLvl") && obj.getArgs().length > 4) {
			Integer selectedSkillId = Integer.parseInt(obj.getArgs()[0]);
			Integer selectedSkillLvl = Integer.parseInt(obj.getArgs()[1]);
			Integer itemId = Integer.parseInt(obj.getArgs()[2]);
			Integer skillToRemoveId = Integer.parseInt(obj.getArgs()[3]);
			Integer skillToRemoveLvl = Integer.parseInt(obj.getArgs()[4]);
			ItemInstance item = player.getInventory().getItemByItemObjId(itemId);
			List<Skill> skills = new ArrayList<Skill>(item.getItemInstanceSkills());
			skills.remove(SkillTable.getInstance().getInfo(skillToRemoveId, skillToRemoveLvl));
			skills.add(SkillTable.getInstance().getInfo(selectedSkillId, selectedSkillLvl));
			player.getInventory().unEquipItem(item);
			item.setItemInstanceSkills(skills, true);
			Log.LogStatsRndSkills("Success:AdminSetSkill:CB", player, CommunityDiabloItems.class.getSimpleName(), item);
		}
		if (obj.getMethod().equals("showAdminRemoveSkill") && obj.getArgs().length > 2) {
			Integer itemId = Integer.parseInt(obj.getArgs()[0]);
			Integer skillToRemoveId = Integer.parseInt(obj.getArgs()[1]);
			Integer skillToRemoveLvl = Integer.parseInt(obj.getArgs()[2]);
			ItemInstance item = player.getInventory().getItemByItemObjId(itemId);
			List<Skill> skills = new ArrayList<Skill>(item.getItemInstanceSkills());
			skills.remove(SkillTable.getInstance().getInfo(skillToRemoveId, skillToRemoveLvl));
			player.getInventory().unEquipItem(item);
			item.setItemInstanceSkills(skills, true);
			Log.LogStatsRndSkills("Success:AdminRemoveSkill:CB", player, "Skill removed - " + skillToRemoveId + " lvl - " + skillToRemoveLvl, item);
		}
		if (obj.getMethod().equals("showAdminAddSkillAndLvl") && obj.getArgs().length > 2) {
			Integer selectedSkillId = Integer.parseInt(obj.getArgs()[0]);
			Integer selectedSkillLvl = Integer.parseInt(obj.getArgs()[1]);
			Integer itemId = Integer.parseInt(obj.getArgs()[2]);
			ItemInstance item = player.getInventory().getItemByItemObjId(itemId);
			List<Skill> skills = new ArrayList<Skill>(item.getItemInstanceSkills());
			if (skills.size() >= StatsRndManager.getMaxSkillsInItem()) {
				obj.addHash("error", "Достигнут предел количества скилов в итеме");
			} else {
				skills.add(SkillTable.getInstance().getInfo(selectedSkillId, selectedSkillLvl));
				player.getInventory().unEquipItem(item);
				item.setItemInstanceSkills(skills, true);
				Log.LogStatsRndSkills("Success:AdminSetSkill:CB", player, CommunityDiabloItems.class.getSimpleName(), item);
			}
		}
		return obj;
	}

	private static void destroyItem(ItemInstance item, Player player) {
		if (!player.getInventory().destroyItem(item, 1L, "SkillEnchantFail"))
		{
			player.sendActionFailed();
			return;
		}
		player.sendPacket(new EnchantResult(1, 0, 0));
	}

	private static void takeFee(Player player, Integer currencyId, Long currentPrice){
		if (currentPrice > 0) {
			ItemInstance item = player.getInventory().getItemByItemId(currencyId);
			item.setCount(player.getInventory().getItemByItemId(currencyId).getCount() - currentPrice);
			item.setJdbcState(JdbcEntityState.UPDATED);
			item.update();
		}
	}

	private static boolean checkMoney(Integer currencyId, Long price, Player player) {
		return price <= 0 || player.getInventory().getItemByItemId(currencyId).getCount() >= price;
	}

	private static List<Skill> parseSkills(String raw){
		List<Skill> result = new ArrayList<>();
		String[] skills = raw.split(";");
		for (String rawSkill : skills) {
			Skill sk = SkillTable.getInstance().getInfo(Integer.parseInt(rawSkill.split(":")[0]), Integer.parseInt(rawSkill.split(":")[1]));
			if (sk != null) {
				result.add(sk);
			}
		}
		return result;
	}

	private static CommunicationObject prepare(CommunicationObject obj, Player player) {
		Integer currencyId = 0;
		Long price = 0l;
		if (obj.getArgs().length > 0) {
			currencyId = Integer.parseInt(obj.getArgs()[0]);
		}
		if (obj.getArgs().length > 1) {
			price = Long.parseLong(obj.getArgs()[1]);
		}
		if (obj.getMethod().equals("doRandom") && obj.getArgs().length > 3){
			Integer itemId = Integer.parseInt(obj.getArgs()[2]);
			Integer skillId = Integer.parseInt(obj.getArgs()[3]);
			Integer skillLvl = Integer.parseInt(obj.getArgs()[4]);
			if (checkMoney(currencyId, price, player)) {
				ItemInstance item = player.getInventory().getItemByItemObjId(itemId);
				item.setRandomCount(item.getRandomCount() +1 );
				takeFee(player, currencyId, price);
			} else {
				obj.addHash("error", "Не хватает "+ player.getInventory().getItemByItemId(currencyId).getName() +" !");
			}
		}
		// Setskill -
		if (obj.getMethod().equals("setSkill") && obj.getArgs().length > 5){
			Integer selectedSkillId = Integer.parseInt(obj.getArgs()[2]);
			Integer selectedSkillLvl = Integer.parseInt(obj.getArgs()[3]);
			Integer itemId = Integer.parseInt(obj.getArgs()[4]);
			Integer skillToRemoveId = Integer.parseInt(obj.getArgs()[5]);
			Integer skillToRemoveLvl = Integer.parseInt(obj.getArgs()[6]);
			ItemInstance item = player.getInventory().getItemByItemObjId(itemId);
			if (checkMoney(currencyId, price, player)) {
				if (player.isInStoreMode()) {
					obj.addHash("error", "Нелья находится в режиме магазина");
					Log.LogStatsRndSkills("Fail:setSkill:StoreMode", player, CommunityDiabloItems.class.getSimpleName(), item);
				} else {
					List<Skill> skills = new ArrayList<Skill>(item.getItemInstanceSkills());
					skills.remove(SkillTable.getInstance().getInfo(skillToRemoveId, skillToRemoveLvl));
					skills.add(SkillTable.getInstance().getInfo(selectedSkillId, selectedSkillLvl));
					player.getInventory().unEquipItem(item);
					item.setItemInstanceSkills(skills, true);
					takeFee(player, currencyId, price);
					Log.LogStatsRndSkills("Success:SetSkill:CB", player, CommunityDiabloItems.class.getSimpleName(), item);
				}
			} else {
				obj.addHash("error", "Не хватает "+ player.getInventory().getItemByItemId(currencyId).getName() +" !");
			}
		}

		if (obj.getMethod().equals("upgradeSkill") && obj.getArgs().length > 4){
			Integer selectedSkillId = Integer.parseInt(obj.getArgs()[2]);
			Integer selectedSkillLvl = Integer.parseInt(obj.getArgs()[3]);
			Integer selectedSkillLvlUp = Integer.parseInt(obj.getArgs()[4]);
			Integer itemId = Integer.parseInt(obj.getArgs()[5]);
			Integer deleteItem = Integer.parseInt(obj.getArgs()[6]);
			ItemInstance item = player.getInventory().getItemByItemObjId(itemId);
			if (checkMoney(currencyId, price, player)) {
				if (player.isInStoreMode()) {
					obj.addHash("error", "Нелья находится в режиме магазина");
					Log.LogStatsRndSkills("Fail:UpgradeSkill:StoreMode", player, CommunityDiabloItems.class.getSimpleName(), item);
				} else {
					if (deleteItem == 0 && item != null) {
						List<Skill> skills = new ArrayList<Skill>(item.getItemInstanceSkills());
						skills.remove(SkillTable.getInstance().getInfo(selectedSkillId, selectedSkillLvl));
						skills.add(SkillTable.getInstance().getInfo(selectedSkillId, selectedSkillLvlUp));
						player.getInventory().unEquipItem(item);
						item.setItemInstanceSkills(skills, true);
						takeFee(player, currencyId, price);
						player.sendPacket(new EnchantResult(0,0,0));
						Log.LogStatsRndSkills("Success:UpgradeSkill:SkillUpgraded:" +currencyId + ":" +price, player, CommunityDiabloItems.class.getSimpleName(), item);
						obj.addHash("upgrade", "ok");
					} else if (deleteItem == 1 && item != null) {
						destroyItem(item, player);
						Log.LogStatsRndSkills("Fail:UpgradeSkill:DestroyItem", player, CommunityDiabloItems.class.getSimpleName(), item);
						obj.addHash("upgrade", "notok");
					} else if (deleteItem == -1 && item != null) {
						Log.LogStatsRndSkills("Success:UpgradeSkill:CheckResult:" +currencyId + ":" +price, player, CommunityDiabloItems.class.getSimpleName(), item);
						obj.addHash("upgrade", "showResult");
					} else if (item == null) {
						obj.addHash("error", "Куда-то пропал предмет !");
						Log.LogStatsRndSkills("Fail:UpgradeSkill:ItemMiss", player, CommunityDiabloItems.class.getSimpleName(), item);
					}
				}
			} else {
				obj.addHash("error", "Не хватает "+ player.getInventory().getItemByItemId(currencyId).getName() +" !");
			}

		}

		if (obj.getMethod().equals("changeSkill") && obj.getArgs().length > 4){
			Integer oldSkillId = Integer.parseInt(obj.getArgs()[2]);
			Integer oldSkillLvl = Integer.parseInt(obj.getArgs()[3]);
			Integer newSkillId = Integer.parseInt(obj.getArgs()[4]);
			Integer newSkillLvl = Integer.parseInt(obj.getArgs()[5]);
			Integer itemId = Integer.parseInt(obj.getArgs()[6]);
			if (checkMoney(currencyId, price, player)) {
				ItemInstance item = player.getInventory().getItemByItemObjId(itemId);
				List<Skill> skills = new ArrayList<Skill>(item.getItemInstanceSkills());
				skills.remove(SkillTable.getInstance().getInfo(oldSkillId, oldSkillLvl));
				skills.add(SkillTable.getInstance().getInfo(newSkillId, newSkillLvl));
				player.getInventory().unEquipItem(item);
				item.setItemInstanceSkills(skills, true);
				takeFee(player, currencyId, price);
				Log.LogStatsRndSkills("Success:ChangeSkill:SkillChanged", player, CommunityDiabloItems.class.getSimpleName(), item);
			} else {
				obj.addHash("error", "Не хватает "+ player.getInventory().getItemByItemId(currencyId).getName() +" !");
			}
		}

		if (obj.getMethod().equals("addSkill") && obj.getArgs().length > 3){
			Integer skillId = Integer.parseInt(obj.getArgs()[2]);
			Integer skillLvl = Integer.parseInt(obj.getArgs()[3]);
			Integer itemId = Integer.parseInt(obj.getArgs()[4]);
			Integer deleteItem = Integer.parseInt(obj.getArgs()[5]);
			if (checkMoney(currencyId, price, player)) {
				ItemInstance item = player.getInventory().getItemByItemObjId(itemId);
				if (deleteItem != 1 && item != null) {
					List<Skill> skills = new ArrayList<Skill>(item.getItemInstanceSkills());
					skills.add(SkillTable.getInstance().getInfo(skillId, skillLvl));
					player.getInventory().unEquipItem(item);
					item.setItemInstanceSkills(skills, true);
					takeFee(player, currencyId, price);
					Log.LogStatsRndSkills("Success:AddSkill:SkillAdded", player, CommunityDiabloItems.class.getSimpleName(), item);
				} else if (item != null) {
					destroyItem(item, player);
					Log.LogStatsRndSkills("Fail:AddSkill:ItemDestroyed", player, CommunityDiabloItems.class.getSimpleName(), item);
				}
			} else {
				obj.addHash("error", "Не хватает "+ player.getInventory().getItemByItemId(currencyId).getName() +" !");
			}

		}

		if (obj.getMethod().equals("resetItem") && obj.getArgs().length > 3){
			List<Skill> skills = parseSkills(obj.getArgs()[2]);
			Integer itemId = Integer.parseInt(obj.getArgs()[3]);
			if (checkMoney(currencyId, price, player)) {
				ItemInstance item = player.getInventory().getItemByItemObjId(itemId);
				player.getInventory().unEquipItem(item);
				item.setItemInstanceSkills(skills, true);
				takeFee(player, currencyId, price);
				Log.LogStatsRndSkills("Success:ItemReset:SkillsReset", player, CommunityDiabloItems.class.getSimpleName(), item);
			} else {
				obj.addHash("error", "Не хватает "+ player.getInventory().getItemByItemId(currencyId).getName() +" !");
			}
		}

		if (currencyId != 0 && currencyId != 57 && player.getInventory().getItemByItemId(currencyId) != null) {
			obj.addHash("currencyId", Integer.toString(currencyId));
			obj.addHash("currencyAmount", Long.toString(player.getInventory().getItemByItemId(currencyId).getCount()));
		}

		return obj;
	}

	@Override
	public void onWriteCommand(Player player, String bypass, String arg1, String arg2, String arg3, String arg4, String arg5)
	{}
}