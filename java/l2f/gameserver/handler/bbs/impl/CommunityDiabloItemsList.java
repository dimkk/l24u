package l2f.gameserver.handler.bbs.impl;

import l2f.commons.dao.JdbcEntityState;
import l2f.commons.l24u.Communication.CommunicationObject;
import l2f.gameserver.Config;
import l2f.gameserver.data.htm.HtmCache;
import l2f.gameserver.handler.bbs.CommunityBoardManager;
import l2f.gameserver.handler.bbs.ICommunityBoardHandler;
import l2f.gameserver.model.Player;
import l2f.gameserver.model.Skill;
import l2f.gameserver.model.items.ItemInstance;
import l2f.gameserver.network.serverpackets.EnchantResult;
import l2f.gameserver.network.serverpackets.ShowBoard;
import l2f.gameserver.scripts.ScriptFile;
import l2f.gameserver.tables.SkillTable;
import l2f.gameserver.utils.Log;
import l2f.gameserver.utils.l24u.Common;
import l2f.gameserver.utils.l24u.diablo.L24uDistribution;
import l2f.gameserver.utils.l24u.diablo.StatsRndManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


/**
 * Community Board page containing Item randomizer and tools
 */
public class CommunityDiabloItemsList implements ScriptFile, ICommunityBoardHandler
{
	private static final Logger _log = LoggerFactory.getLogger(CommunityDiabloItemsList.class);

	@Override
	public void onLoad()
	{
		if(Config.COMMUNITYBOARD_ENABLED)
		{
			_log.info("CommunityBoard: DiabloItemsListLoaded.");
			//CommunityBoardManager.getInstance().registerHandler(this);
		}
	}

	@Override
	public void onReload()
	{
		if(Config.COMMUNITYBOARD_ENABLED) {}
			//CommunityBoardManager.getInstance().removeHandler(this);
	}

	@Override
	public void onShutdown()
	{}

	@Override
	public String[] getBypassCommands()
	{
		return new String[] {
				"_friendlist_",
				"diablo_itemsList",
		};
	}

	@Override
	public void onBypassCommand(Player player, String bypass) {
		if (!Config.DIABLO_RND_STATS) {
			return;
		}
		CommunicationObject obj;
		if(bypass.contains("friendlist"))
		{
			obj = new CommunicationObject("statsRnd", "itemsList");
		} else {
			obj = Common.getCommunicationObjectFromBypass(bypass);
		}

		obj = prepareCommunication(obj, player);
		new L24uDistribution(player).communicate(obj);
		return;
	}

	public static CommunicationObject prepareCommunication(CommunicationObject obj, Player player) {
		obj.addHash("playerId", Integer.toString(player.getObjectId()));
		obj.addHash("lvl", Integer.toString(player.getLevel()));
		obj.addHash("currentAdena", Long.toString(player.getAdena()));
		obj.addHash("isAdmin", Boolean.toString(player.isGM()));
		obj.addHash("items", StatsRndManager.getItemsToSend(player));

		return obj;
	}

	private static void destroyItem(ItemInstance item, Player player) {
		if (!player.getInventory().destroyItem(item, 1L, "ItemsList:Destroy"))
		{
			player.sendActionFailed();
			return;
		}
	}

	private static boolean checkMoney(Integer currencyId, Long price, Player player) {
		return price <= 0 || player.getInventory().getItemByItemId(currencyId).getCount() >= price;
	}

	private static List<Skill> parseSkills(String raw){
		List<Skill> result = new ArrayList<>();
		String[] skills = raw.split(";");
		for (String rawSkill : skills) {
			Skill sk = SkillTable.getInstance().getInfo(Integer.parseInt(rawSkill.split(":")[0]), Integer.parseInt(rawSkill.split(":")[1]));
			if (sk != null) {
				result.add(sk);
			}
		}
		return result;
	}

	private static CommunicationObject prepare(CommunicationObject obj, Player player) {
		Integer currencyId = 0;
		Long price = 0l;
		if (obj.getArgs().length > 0) {
			currencyId = Integer.parseInt(obj.getArgs()[0]);
		}
		if (obj.getArgs().length > 1) {
			price = Long.parseLong(obj.getArgs()[1]);
		}

		return obj;
	}

	@Override
	public void onWriteCommand(Player player, String bypass, String arg1, String arg2, String arg3, String arg4, String arg5)
	{}
}