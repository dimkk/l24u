package l2f.commons.util.concurrent;

import l2f.commons.configuration.ExProperties;

import static java.lang.System.getenv;

/**
 * Created by dimkk on 14/07/16.
 */
public class Props {
    private static String getPropertyWithEnvRespect(ExProperties props, String prop, String def)
    {
        String result = getenv(prop) != null
                ? getenv(prop)
                : props.getProperty(prop, def);
        return result;

    }
}
