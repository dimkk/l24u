package l2f.commons.data.xml;

import l2f.gameserver.utils.l24u.diablo.L24uDistribution;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;

import java.io.File;
import java.util.Collection;

public abstract class AbstractDirParser<H extends AbstractHolder> extends AbstractParser<H>
{
	protected AbstractDirParser(H holder)
	{
		super(holder);
	}

	public abstract File getXMLDir();

	public abstract boolean isIgnored(File f);

	public abstract String getDTDFileName();

	@Override
	protected final void parse()
	{
		File dir = getXMLDir();

		if (!dir.exists())
		{
			warn("Dir " + dir.getAbsolutePath() + " not exists");
			return;
		}

		File dtd = new File(dir, getDTDFileName());
		if (!dtd.exists())
		{
			info("DTD file: " + dtd.getName() + " not exists.");
			return;
		}

		initDTD(dtd);


		try
		{
			Collection<File> files = FileUtils.listFiles(dir, FileFilterUtils.suffixFileFilter(".xml"),
					FileFilterUtils.directoryFileFilter());

			if (this.getClass().getName().contains("ItemParser")) {
				files.addAll(L24uDistribution.getDistributedDataSync("items"));
			}
			if (this.getClass().getName().contains("NpcParser")) {
				files.addAll(L24uDistribution.getDistributedDataSync("npc"));
			}

			for (File f : files)
				if (!f.isHidden())
					if (!isIgnored(f))
						try
						{
							parseCrypted(f);
						}
						catch (Exception e)
						{
							info("Exception: " + e + " in file: " + f.getName(), e);
						}
		}
		catch (RuntimeException e)
		{
			error("Exception in AbstractDirParser ", e);
		}
	}
}
