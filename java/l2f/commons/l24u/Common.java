package l2f.commons.l24u;

import l2f.commons.l24u.Communication.CommunicationObject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Dima on 01.09.2016.
 */
public class Common {
    protected static final Logger _log = LoggerFactory.getLogger("l24uCommon");


    public static boolean contIgnoreCase(String source, String subj){
        return StringUtils.containsIgnoreCase(source, subj);
    }

    public static int getRndBetween(int low, int high){
        Random r = new Random();
        if (low == 1 && high == 2) return r.nextBoolean() ? low : high;
        int Result = r.nextInt(high-low) + low;
        return Result;
    }

    public static double calculateAverage(List <Integer> marks) {
        Integer sum = 0;
        if(!marks.isEmpty()) {
            for (Integer mark : marks) {
                sum += mark;
            }
            return sum.doubleValue() / marks.size();
        }
        return sum;
    }


    public static void writeToFile(String fileName, String contents){
        try {
            FileUtils.write(new File(fileName), contents, "UTF-8");
        } catch (IOException e) {
            _log.error("errrr", e);
        }
    }

    /**
     * Парсит строку вида diablo_section_method_arg1 arg2 arg3 ... в объект CommunicationObject
     * @param bypass diablo_section_method_arg1 arg2 arg3
     * @return
     */
    public static CommunicationObject getCommunicationObjectFromBypass(String bypass) {
        String command = bypass.trim();
        String[] array = command.split("_").clone();
        String section = array[1];
        String method = array[2];
        array = ArrayUtils.removeElement(array, array[0]); //diablo
        array = ArrayUtils.removeElement(array, section);//section
        array = ArrayUtils.removeElement(array, method);//method
        if (array.length > 0){
            String lastElement = array[array.length - 1];
            String[] spaceArgs = lastElement.split(" ");
            if (spaceArgs.length > 1)
                array = spaceArgs;
        }
        CommunicationObject obj = new CommunicationObject(section, method, array);
        return obj;
    }
}
