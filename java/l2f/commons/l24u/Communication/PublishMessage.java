package l2f.commons.l24u.Communication;

/**
 * Created by dimkk on 2017-02-13.
 */
import com.google.gson.Gson;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PublishMessage {
    private static final Logger _log = LoggerFactory.getLogger("MQ_PUBLISHER");
    private static void doPublish(String message, String queueName){
        try {
            Connection connection = ConnectionService.getFactory().newConnection();
            Channel channel = connection.createChannel();

            channel.exchangeDeclare(queueName, "fanout");

            channel.basicPublish(queueName, "", null, message.getBytes());
            //System.out.println(" [x] Sent '" + message + "'");

            channel.close();
            connection.close();
        } catch (Exception ex) {
            _log.error("err", ex);
        }
    }

    public static void call(l2f.commons.l24u.Communication.CommunicationObject message, String exchange) {
        String json = new Gson().toJson(message);
        //String queueName = Config.MQ_QUEUE_NAME+"chatout";
        doPublish(json, exchange);
    }
}