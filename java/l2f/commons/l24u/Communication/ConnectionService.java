package l2f.commons.l24u.Communication;

import com.rabbitmq.client.ConnectionFactory;

/**
 * Created by dimkk on 2017-02-13.
 */
public class ConnectionService {
    private static ConnectionFactory factory;
    public static ConnectionFactory getFactory() {
        return factory;
    }
    public static void initFactory(String host, String login, String password) {
        factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setUsername(login);
        factory.setPassword(password);
    }
}
