package l2f.commons.l24u.Communication;

/**
 * Created by dimkk on 2017-02-13.
 */

import com.rabbitmq.client.*;
import l2f.gameserver.utils.l24u.diablo.L24uDistribution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class RecieveMessage {
    private static final Logger _log = LoggerFactory.getLogger("MQ_RECIEVER");
    private String queue;

    public RecieveMessage(String queue) {
        this.queue = queue;
        _log.info(queue + " listening");
    }

    public void startRecieve(boolean isGame){
        try {
            String queueName = queue;
            Connection connection = ConnectionService.getFactory().newConnection();
            Channel channel = connection.createChannel();
            channel.exchangeDeclare(queueName, "fanout");
            channel.queueDeclare(queueName, false, false, false, null);
            channel.queueBind(queueName, queueName, queueName);

            //System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

            Consumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope,
                                           AMQP.BasicProperties properties, byte[] body) throws IOException {
                    String message = new String(body, "UTF-8");
                    if (isGame)
                        L24uDistribution.parseAndRouteMessage(message);
                    else {
                        l2f.loginserver.l24u.L24uDistribution.parseAndRouteMessage(message);
                    }
                }
            };
            channel.basicConsume(queueName, true, consumer);

        } catch (Exception ex) {
            _log.error("err", ex);
            //new RecieveMessage(this.queue).startRecieve(isGame);
        }
    }
}