package l2f.commons.l24u.Communication;

import com.google.gson.GsonBuilder;

/**
 * Created by dimkk on 2017-03-06.
 */
public class l24uDistrShared {
    public static l2f.commons.l24u.Communication.CommunicationObject getCommunicationObjectFromString(String message){
        l2f.commons.l24u.Communication.CommunicationObject result = null;
        GsonBuilder gson = new GsonBuilder();
        result = gson.create().fromJson(message, l2f.commons.l24u.Communication.CommunicationObject.class);
        return result;
    }
}
