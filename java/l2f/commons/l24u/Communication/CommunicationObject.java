package l2f.commons.l24u.Communication;

import java.util.HashMap;
import java.util.List;

/**
 * Created by dimkk on 11/29/2016.
 */
public class CommunicationObject {
    private String section;

    public CommunicationObject(String section, String method, String[] args) {
        this.setSection(section);
        this.setMethod(method);
        this.setArgs(args);
    }

    public CommunicationObject(String section, String method) {
        this.setSection(section);
        this.setMethod(method);
        this.setArgs(new String[]{});
    }

    private String method;
    private String[] args;
    private HashMap<String, String> hash = new HashMap<>();

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String[] getArgs() {
        return args;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }

    public HashMap<String, String> getHash() {
        return hash;
    }
    public String getHashVal(String key) {
        return hash.get(key);
    }

    public void setHash(HashMap<String, String> hash) {
        this.hash = hash;
    }

    public void addHash(String key, String val) {
        if (this.hash.containsKey(key)) this.hash.remove(key);
        this.hash.put(key, val);
    }

    public void removeByKey(String key) {
        try {
            this.hash.remove(key);
        } catch(Exception ex) {

        }

    }
}
