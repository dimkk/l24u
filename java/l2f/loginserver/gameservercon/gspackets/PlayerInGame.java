package l2f.loginserver.gameservercon.gspackets;

import l2f.loginserver.gameservercon.GameServer;
import l2f.loginserver.gameservercon.ReceivablePacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PlayerInGame extends ReceivablePacket
{
	private final static Logger _log = LoggerFactory.getLogger(PlayerInGame.class);
	private String account;

	@Override
	protected void readImpl()
	{
		account = readS();
	}

	@Override
	protected void runImpl()
	{
		_log.info("PlayerInGame");
		GameServer gs = getGameServer();
		if (gs.isAuthed())
			gs.addAccount(account);
	}
}
