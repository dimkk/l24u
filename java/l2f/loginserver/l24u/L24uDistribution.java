package l2f.loginserver.l24u;

import l2f.loginserver.accounts.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static l2f.commons.l24u.Communication.l24uDistrShared.getCommunicationObjectFromString;

/**
 * Created by dimkk on 11/29/2016.
 */
public class L24uDistribution {
    protected static final Logger _log = LoggerFactory.getLogger("L24uDistribution");

    public static void parseAndRouteMessage(String message){
        try {
            l2f.commons.l24u.Communication.CommunicationObject obj = getCommunicationObjectFromString(message);
            if (obj == null) return;
            if (obj.getSection().equalsIgnoreCase("lk")) {
                if (obj.getMethod().equalsIgnoreCase("createAccount")) {
                    String login = obj.getHashVal("login");
                    String password = obj.getHashVal("password");
                    String email = obj.getHashVal("email");
                    String actor = obj.getHashVal("actor");
                    _log.info(String.format("%s created account %s, with site [uid:login:email] [%s]", actor, login, email));
                    if ((login.matches(l2f.loginserver.Config.ANAME_TEMPLATE)) && (password.matches(l2f.loginserver.Config.APASSWD_TEMPLATE))) {
                        Account account = new Account(login);
                        account.restore();
                        account.setAllowedIP("");
                        account.setAllowedHwid("");
                        account.setPasswordHash(password);
                        account.setEmail(email);
                        try {
                            account.save();
                        } catch (Exception ex) {
                            _log.error("err", ex);
                            if (actor.equalsIgnoreCase("admin") || actor.equalsIgnoreCase("superadmin") || email.equalsIgnoreCase(account.getEmail())) {
                                account.setEmail(email);
                                account.setPasswordHash(password);
                                account.update();
                            }
                        }
                    }

                }
                if (obj.getMethod().equalsIgnoreCase("deleteAccount")) {
                    String login = obj.getHashVal("login");
                    String actor = obj.getHashVal("actor");
                    _log.info(String.format("%s deleted account %s, with site", actor, login));
                    Account account = new Account(login);
                    account.restore();
                    account.setPasswordHash("randomPassl24u" + l2f.commons.l24u.Common.getRndBetween(500, 500000));
                    account.update();

                }
            }
        } catch (Exception ex) {
            _log.warn("communication error", ex);
        }
    }
}

