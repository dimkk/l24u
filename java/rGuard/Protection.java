package rGuard;

import java.io.IOException;

import l2f.gameserver.handler.admincommands.AdminCommandHandler;
import rGuard.crypt.BlowfishEngine;
import rGuard.hwidmanager.HWIDAdminBan;
import rGuard.hwidmanager.HWIDBan;
import rGuard.hwidmanager.HWIDManager;
import rGuard.network.ProtectionManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Protection
{
	private static final Logger _log = LoggerFactory.getLogger(Protection.class);
	private static byte[] _key = new byte[16];

	public static void Init()
	{
		ConfigProtection.load();
		if(isProtectionOn())
		{_log.info("************[ Protection System: Loading... ]*************");
			HWIDBan.getInstance();
			HWIDManager.getInstance();
			//ProtectionManager.getInstance();
			AdminCommandHandler.getInstance().registerAdminCommandHandler(new HWIDAdminBan());
		_log.info("************[ Protection System: Protection ON ]*************");}
	}

	public static boolean isProtectionOn()
	{
		if(ConfigProtection.ALLOW_GUARD_SYSTEM)
			return true;
		return false;
	}

	public static byte[] getKey(byte[] key)
	{
		byte[] bfkey = {110, 36, 2, 15, -5, 17, 24, 23, 18, 45, 1, 21, 122, 16, -5, 12};
		try
		{
			BlowfishEngine bf = new BlowfishEngine();
			bf.init(true, bfkey);
			bf.processBlock(key, 0, _key, 0);
			bf.processBlock(key, 8, _key, 8);
		}
		catch(IOException e)
		{
			_log.info("Bad key!!!");
		}
		return _key;
	}

	public static void main(String[] args){
		Init();
	}
}