CREATE TABLE `accounts` (
	`login` VARCHAR(32) NOT NULL,
	`password` VARCHAR(255) NOT NULL,
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`access_level` INT(11) NOT NULL DEFAULT '0',
	`email` VARCHAR(255) NULL DEFAULT '0',
	`ban_expire` INT(11) NOT NULL DEFAULT '0',
	`allow_ip` VARCHAR(255) NULL DEFAULT NULL,
	`allow_hwid` VARCHAR(255) NULL DEFAULT NULL,
	`bonus` DOUBLE NOT NULL DEFAULT '1',
	`bonus_expire` INT(11) NOT NULL DEFAULT '0',
	`last_server` INT(11) NOT NULL DEFAULT '0',
	`last_ip` VARCHAR(15) NULL DEFAULT NULL,
	`last_access` INT(11) NOT NULL DEFAULT '0',
	`bot_report_points` INT(10) NOT NULL DEFAULT '7',
	`points` INT(11) NOT NULL DEFAULT '0',
	`secondaryPassword` VARCHAR(400) NULL DEFAULT '',
	PRIMARY KEY (`id`, `login`),
	INDEX `last_ip` (`last_ip`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
