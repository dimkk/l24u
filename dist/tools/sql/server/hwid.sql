CREATE TABLE IF NOT EXISTS `hwid` (
 `HWID` VARCHAR( 32 ) default NULL ,
 `threat` varchar(32) default NULL,
 `first_time_played` BIGINT UNSIGNED NOT NULL DEFAULT 0,
 `total_time_played` BIGINT UNSIGNED NOT NULL DEFAULT 0,
 `poll_answer` INT DEFAULT 0,
  `warnings` INT UNSIGNED NOT NULL DEFAULT 0,
   `seenChangeLog` INT UNSIGNED NOT NULL DEFAULT 0,
    `banned` INT UNSIGNED NOT NULL DEFAULT 0,
 `comments` varchar(255) default ''
) ENGINE = MYISAM;