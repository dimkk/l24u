CREATE TABLE `auctions` (
  `auction_id` int(35) NOT NULL UNIQUE DEFAULT '0',
  `seller_object_id` int(11) NOT NULL DEFAULT '0',
  `seller_name` varchar(100) NOT NULL DEFAULT '',
  `store_text` varchar(300) NOT NULL DEFAULT '',
  `item_object_id` int(11) NOT NULL DEFAULT '0',
  `price_per_item` bigint NOT NULL DEFAULT '0',
  PRIMARY KEY (`auction_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

