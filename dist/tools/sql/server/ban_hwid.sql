SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for ban_hwid
-- ----------------------------
DROP TABLE IF EXISTS `ban_hwid`;
CREATE TABLE `ban_hwid` (
  `hwid` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records 
-- ----------------------------
