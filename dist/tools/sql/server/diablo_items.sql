CREATE TABLE `diablo_items` (
	`objectId` INT(11) NOT NULL,
	`skillsLevelsJson` TEXT NOT NULL,
	`randomCount` INT(11) NOT NULL DEFAULT '0',
	`statsUpgradeCount` INT(11) NOT NULL DEFAULT '0',
	`currentPrice` INT(11) NOT NULL DEFAULT '0',
	PRIMARY KEY (`objectId`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
