#!/bin/bash
java -server -Dfile.encoding=UTF-8 -Xmx1G -cp "loginserver/config:/var/l2/dist/libs/*" l2f.loginserver.AuthServer
#nohup java -server -Dfile.encoding=UTF-8 -Xmx13G -cp "gameserver/config:/var/l2/dist/libs/*" com.lameguard.LameGuard l2f.gameserver.GameServer > gamelog.log 2>&1

awk '{while(match($0,"[$]{[^}]*}")) {var=substr($0,RSTART+2,RLENGTH -3);gsub("[$]{"var"}",ENVIRON[var])}}1' < input.txt > output.txt