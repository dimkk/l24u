#!/bin/bash

while :;
do
	java -server -Dfile.encoding=UTF-8 -Xmx1G -cp "config:../libs/*" l2f.loginserver.AuthServer

	[ $? -ne 2 ] && break
	sleep 10;
done
