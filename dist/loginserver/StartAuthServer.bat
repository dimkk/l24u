@echo off
COLOR 0C
title FandC: AuthServer Console
:start
echo FandC AuthServer.
echo.

REM ������ ������
set JAVA_OPTS=%JAVA_OPTS% -XX:PermSize=128m
set JAVA_OPTS=%JAVA_OPTS% -XX:MaxPermSize=256m


java -Xbootclasspath/p:../jsr167.jar -server -agentlib:hprof -Dfile.encoding=UTF-8  -Xdebug -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5000 -cp config;../libs/* l2f.loginserver.AuthServer

REM Debug ...
REM java -Dfile.encoding=UTF-8 -cp config;./libs/* l2f.gameserver.GameServer

if ERRORLEVEL 2 goto restart
if ERRORLEVEL 1 goto error
goto end
:restart
echo.
echo Server restarted ...
echo.
goto start
:error
echo.
echo Server terminated abnormaly ...
echo.
:end
echo.
echo Server terminated ...
echo.

pause