<html noscrollbar>
<title>Сообщество Lineage 2 for you | L24u</title>
<body><br>
<table width=755>
	<tr>
		<center><td align=center valign=top>
			<table border=0 cellpadding=0 cellspacing=0 width=769 height=492 background="l2ui_ct1.Windows_DF_TooltipBG">
				<tr>
					<td valign="top" align="center">
						<table width=755 height=468>
							<tr>
								<td width=755>
									<br>
									<table border=0 cellspacing=0 cellpadding=0 width=755>
										<tr>
											<td width=755 height=25 align=center valign=top>
												<font name="hs12">Рейты сервера</font>
											</td>
										</tr>
										<tr>
											<td width=755>
												<table width=755 border=0>
													<tr>
														<td width=20></td>
														<td width=755 height=65 align=center valign=top>
															<br>
															<table width=500>
																<tr>
																	<td width=230 height=52 align=center valign=top>
																		<table border=0 width=230 height=46 cellspacing=4 cellpadding=3 bgcolor=666666>
																			<tr>
																				<td width=40 align=right valign=top>
																					<img src="icon.etc_exp_point_i00" width=32 height=32>
																				</td>
																				<td width=210 align=left valign=top>
																					<font color="LEVEL">XP/SP</font><br1> <font color=8DB600>Рейт:</font> ${exp}x
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td width=230 height=52 align=center valign=top>
																		<table border=0 width=230 height=46 cellspacing=4 cellpadding=3 bgcolor=333333>
																			<tr>
																				<td width=40 align=right valign=top>
																					<img src="icon.etc_quest_add_reward_i00" width=32 height=32>
																				</td>
																				<td width=210 align=left valign=top>
																					<font color="LEVEL">Адена</font><br1> <font color=8DB600>Рейт:</font> ${adena}x
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td width=230 height=52 align=center valign=top>
																		<table border=0 width=230 height=46 cellspacing=4 cellpadding=3 bgcolor=666666>
																			<tr>
																				<td width=40 align=right valign=top>
																					<img src="icon.etc_quest_account_reward_i00" width=32 height=32>
																				</td>
																				<td width=210 align=left valign=top>
																					<font color="LEVEL">Drop/Spoil</font><br1> <font color=8DB600>Рейт:</font> ${drop}x
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td width=230 height=52 align=center valign=top>
																		<table border=0 width=230 height=46 cellspacing=4 cellpadding=3 bgcolor=333333>
																			<tr>
																				<td width=40 align=right valign=top>
																					<img src="L2UI_CH3.QuestBtn_light" width=32 height=32>
																				</td>
																				<td width=210 align=left valign=top>
																					<font color="LEVEL">Дроп квест итемов</font><br1> <font color=8DB600>Рейт:</font> ${questItems}x
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td width=230 height=52 align=center valign=top>
																		<table border=0 width=230 height=46 cellspacing=4 cellpadding=3 bgcolor=666666>
																			<tr>
																				<td width=40 align=right valign=top>
																					<img src="icon.ench_wp_stone_i04" width=32 height=32>
																				</td>
																				<td width=210 align=left valign=top>
																					<font color="LEVEL">Заточка безопасная/максимальная</font><br1> <font color=8DB600>Значение:</font> +${enchantSafeMax}/+${enchantMax}
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td width=230 height=52 align=center valign=top>
																		<table border=0 width=230 height=46 cellspacing=4 cellpadding=3 bgcolor=333333>
																			<tr>
																				<td width=40 align=right valign=top>
																					<img src="icon.etc_scroll_of_enchant_weapon_i05" width=32 height=32>
																				</td>
																				<td width=210 align=left valign=top>
																					<font color="LEVEL">Обычный свиток</font><br1> <font color=8DB600>Шанс:</font> ${startScrollEnchantChance}%
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td width=230 height=52 align=center valign=top>
																		<table border=0 width=230 height=46 cellspacing=4 cellpadding=3 bgcolor=666666>
																			<tr>
																				<td width=40 align=right valign=top>
																					<img src="icon.etc_blessed_scrl_of_ench_wp_s_i05" width=32 height=32>
																				</td>
																				<td width=210 align=left valign=top>
																					<font color="LEVEL">Благославенный свиток</font><br1> <font color=8DB600>Шанс:</font>  ${startBlessScrollEnchantChance}%
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td width=230 height=52 align=center valign=top>
																		<table border=0 width=230 height=46 cellspacing=4 cellpadding=3 bgcolor=333333>
																			<tr>
																				<td width=40 align=right valign=top>
																					<img src="icon.etc_wind_stone_i00" width=32 height=32>
																				</td>
																				<td width=210 align=left valign=top>
																					<font color="LEVEL">Камни атрибута</font><br1> <font color=8DB600>Шанс:</font> ${attrChance}%
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td width=230 height=52 align=center valign=top>
																		<table border=0 width=230 height=46 cellspacing=4 cellpadding=3 bgcolor=666666>
																			<tr>
																				<td width=40 align=right valign=top>
																					<img src="icon.etc_wind_crystal_i00" width=32 height=32>
																				</td>
																				<td width=210 align=left valign=top>
																					<font color="LEVEL">Камни атрибута 150+</font><br1> <font color=8DB600>Шанс:</font> ${attr300Chance}%
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td width=230 height=50 align=center valign=top>
																		<table border=0 width=230 height=46 cellspacing=4 cellpadding=3 bgcolor=333333>
																			<tr>
																				<td width=40 align=right valign=top>
																					<img src="icon.skill1450" width=32 height=32>
																				</td>
																				<td width=210 align=left valign=top>
																					<font color="LEVEL">Бафы/Танцы</font><br1> <font color=8DB600>Значения:</font> 24/12
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td width=230 height=50 align=center valign=top>
																		<table border=0 width=230 height=46 cellspacing=4 cellpadding=3 bgcolor=666666>
																			<tr>
																				<td width=40 align=right valign=top>
																					<img src="icon.skill5239" width=32 height=32>
																				</td>
																				<td width=210 align=left valign=top>
																					<font color="LEVEL">Время бафов</font><br1> <font color=8DB600>Значение:</font> 2 часа
																				</td>
																			</tr>
																		</table>
																	</td>
																	<td width=230 height=50 align=center valign=top>
																		<table border=0 width=230 height=46 cellspacing=4 cellpadding=3 bgcolor=333333>
																			<tr>
																				<td width=40 align=right valign=top>
																					<img src="icon.skill0219" width=32 height=32>
																				</td>
																				<td width=210 align=left valign=top>
																					<font color="LEVEL">Максимальное кол-во окон</font><br1> <font color=8DB600>Значание:</font> ${windowCount}
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															<table>
																<tr>
																	<td width=719 height=110 align=center valign=top>
																		<table width=719 border=0 cellspacing=4 cellpadding=3 bgcolor=FF9933>
																			<tr>
																				<td width=40 align=right valign=top>
																					<img src="icon.etc_magic_coin_gold_i02" width=32 height=32>
																				</td>
																				<td width=530 align=left valign=top>
																					<font color="LEVEL">Олимпиада</font>
																					Период олимпа - 2 недели, Герои - 1 и 15 числа каждого месяца<br1>
																					Защита по HWID и система антифид, усложнят жизнь читерам.<br1>
																					Стартовых очков - 50, по 25 каждую неделю<br1>
																					Максимальная заточка на олимпе - +6 (даже если есть итем больше - она сбросится)
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td> 
										</tr>
									</table>
									<br><br>
									<table width=755 border=0 height=40>
										<tr>
											<td width=600 align=center valign=top>
												<button action="bypass _bbshome" width=32 height=32 back="L2UI_CT1.MiniMap_DF_MinusBtn_Red_Down" fore="L2UI_CT1.MiniMap_DF_MinusBtn_Red" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td></center>
	</tr>
</table>
</body></html>
