#!/bin/bash

while :;
do
java -server -Dfile.encoding=UTF-8 -Xmx9G -XX:PermSize=1G -XX:+UseConcMarkSweepGC -XX:+UseTLAB -XX:+CMSIncrementalMode -XX:+CMSIncrementalPacing -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:./gc.log -cp config:./lameguard-1.9.5.jar:./libs/* com.lameguard.LameGuard l2f.gameserver.GameServer > log/stdout.log 2>&1
        [ $? -ne 2 ] && break
        sleep 30;
done
